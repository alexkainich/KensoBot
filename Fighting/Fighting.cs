﻿using System;
using System.Collections.Generic;
using Randomization;
using KensobyEF;

namespace Fighting
{
    public class Fight
    {
        public static Tuple<int, int> PveFight(Dictionary<string, string> heroElements, Dictionary<string, string> choiceElements)
        {
            //hero conventions
            int heroHealth = Int32.Parse(heroElements["health"]);
            int heroMaxHealth = Int32.Parse(heroElements["maxhealth"]);
            int heroArmor = Int32.Parse(heroElements["arArmor"]);
            int heroLifeS = Int32.Parse(heroElements["wpLifes"]);
            int heroLevel = Int32.Parse(heroElements["level"]);
            int[] heroStats = { heroHealth, heroMaxHealth, heroArmor, heroLifeS, heroLevel };
            string heroDamage = heroElements["wpDmg"];

            //map choice conventions
            int monsterHealth = Int32.Parse(choiceElements["health"]);
            int monsterArmor = Int32.Parse(choiceElements["ac"]);
            int monsterLifeS = Int32.Parse(choiceElements["lifes"]);
            int monsterLevel = Int32.Parse(choiceElements["level"]);
            int[] monsterStats = { monsterHealth, monsterHealth, monsterArmor, monsterLifeS, monsterLevel }; //second "monsterHealth" is used as maxHealth
            string monsterDamage = choiceElements["dmg"];

            Tuple<int, int> healths = fight(heroStats, heroDamage, monsterStats, monsterDamage);
            return Tuple.Create(healths.Item1, healths.Item2);
        }

        public static Tuple<int,int> PvpFight(Dictionary<string, string> hero1Elements, Dictionary<string, string> hero2Elements)
        {
            int hero1Health = Int32.Parse(hero1Elements["health"]);
            int hero1MaxHealth = Int32.Parse(hero1Elements["maxhealth"]);
            int hero1Armor = Int32.Parse(hero1Elements["arArmor"]);
            int hero1LifeS = Int32.Parse(hero1Elements["wpLifes"]);
            int hero1Level = Int32.Parse(hero1Elements["level"]);
            int[] hero1Stats = { hero1Health, hero1MaxHealth, hero1Armor, hero1LifeS, hero1Level };
            string hero1Damage = hero1Elements["wpDmg"];

            int hero2Health = Int32.Parse(hero2Elements["health"]);
            int hero2MaxHealth = Int32.Parse(hero2Elements["maxhealth"]);
            int hero2Armor = Int32.Parse(hero2Elements["arArmor"]);
            int hero2LifeS = Int32.Parse(hero2Elements["wpLifes"]);
            int hero2Level = Int32.Parse(hero2Elements["level"]);
            int[] hero2Stats = { hero2Health, hero2MaxHealth, hero2Armor, hero2LifeS, hero2Level };
            string hero2Damage = hero2Elements["wpDmg"];

            Tuple<int,int> heroHealthAfterPvP = fight(hero1Stats, hero1Damage, hero2Stats, hero2Damage);
            return Tuple.Create(heroHealthAfterPvP.Item1, heroHealthAfterPvP.Item2);
        }
        
        public static Tuple<int,int> fight(int[] heroStats, string heroDamage, int[] monsterStats, string monsterDamage)
        {
            Random rnd = new Random();
            int randomNum = rnd.Next(1, 3);

            int heroHealth = heroStats[0];
            int heroMaxHealth = heroStats[1];
            int heroArmor = heroStats[2];
            int heroLifeS = heroStats[3];
            int heroLevel = heroStats[4];

            int monsterHealth = monsterStats[0];
            int monsterMaxHealth = monsterStats[1];
            int monsterArmor = monsterStats[2];
            int monsterLifeS = monsterStats[3];
            int monsterLevel = monsterStats[4];

            int higherLevel;
            if (heroLevel > monsterLevel)
                higherLevel = 1;
            else if (monsterLevel > heroLevel)
                higherLevel = 2;
            else
                higherLevel = 0;

            while (heroHealth > 0 && monsterHealth > 0)
            {
                //monster attacks first
                if (randomNum == 1)
                {
                    int damageDone = Math.Max((higherLevel == 2 || higherLevel == 0) ? 1 : 0, dmgCalculate(monsterDamage) - heroArmor);
                    heroHealth -= damageDone;
                    monsterHealth = Math.Min(monsterMaxHealth, monsterHealth + (int)(damageDone * monsterLifeS / 100.0));

                    if (heroHealth <= 0)
                        break;

                    randomNum = 2;
                }
                //hero attacks first
                else
                {
                    int damageDone = Math.Max((higherLevel == 1 || higherLevel == 0) ? 1 : 0, dmgCalculate(heroDamage) - monsterArmor);
                    monsterHealth -= damageDone;
                    heroHealth = Math.Min(heroMaxHealth, heroHealth + (int)(damageDone * heroLifeS / 100.0));

                    if (monsterHealth <= 0)
                        break;

                    randomNum = 1;
                }
            }

            if (heroHealth <= 0)
            {
                heroHealth = 0;
            }
            if (monsterHealth <= 0)
            {
                monsterHealth = 0;
            }

            return Tuple.Create(heroHealth, monsterHealth);
        }

        public static int dmgCalculate(string dmgFormula)
        {
            string[] myDmg = dmgFormula.Split('-');
            int min = Int32.Parse(myDmg[0]);
            int max = Int32.Parse(myDmg[1]);

            Random rnd = new Random();
            int damage = 0;
            damage = rnd.Next(min, max + 1);

            return damage;
        }
    }
}

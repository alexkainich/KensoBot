﻿using System;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing.Drawing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Transforms;
using SixLabors.ImageSharp.Processing.Text;
using SixLabors.Primitives;
using System.Numerics;
using SixLabors.Shapes;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using SixLabors.Fonts;
using Randomization;

namespace Graphics
{
    public static class GenerateMap
    {
        public static string Generate(string guildId, string userIdMap)
        {
            string useridmap = userIdMap;
            Hero myHero = HeroFunctions.GetHero(new KensobyContext(), guildId); ;
            Map myMap = MapFunctions.GetMap(new KensobyContext(), guildId);

            //Hero items level
            int[] heroInfo = new int[] { Int32.Parse(Conventions.GetHeroElements(myHero)["wpLevel"]),
                                         Int32.Parse(Conventions.GetHeroElements(myHero)["arLevel"]) };

            //Load images
            var img = Image.Load($"{myMap.Background}");
            var heroSprite = Image.Load($"{ myMap.Middle }");
            var statsSprite = Image.Load(@"Other/stats.png");

            //Get sprite info
            string[,] spriteInfo = GetSpriteInfo(myMap);

            //config
            int padding = img.Width / 11; //This is the padding between the sprites and the border of the background. "11" is just arbitrary
            string fontType = "Arial";
            Font font = SystemFonts.CreateFont(fontType, 16, FontStyle.Bold); //DejaVu Sans for ubuntu, Arial for Windows

            //place sprites
            img = PlaceSprites(img, font, spriteInfo, heroInfo, fontType, padding);

            //place hero stats
            img = PlaceHeroStats(img, statsSprite, font, guildId);

            //place our hero
            img = PlaceHero(img, heroSprite, statsSprite.Width, padding);

            //save the map
            string datenow = DateTime.Now.Date.Day.ToString() + DateTime.Now.Date.Month.ToString() + DateTime.Now.Date.Year.ToString() + DateTime.Now.TimeOfDay.Hours.ToString() + DateTime.Now.TimeOfDay.Minutes.ToString() + DateTime.Now.TimeOfDay.Seconds.ToString();
            img.Save(@"Map/" + useridmap + datenow + ".png");

            return useridmap + datenow;
        }

        //[,0]:spriteImages ,[,1]:spriteNames, [,2]:spriteHealths , [,3]:spriteLevels, [,4]:spriteTypes
        private static string[,] GetSpriteInfo(Map myMap)
        {
            //get images
            string[] spriteImages = new string[] { myMap.SpriteOne, myMap.SpriteTwo, myMap.SpriteThree, myMap.SpriteFour };
            spriteImages = spriteImages.Where(s => !string.IsNullOrEmpty(s)).ToArray();

            //get sprite details from map
            string[] temp = new string[] { myMap.ChooseOne, myMap.ChooseTwo, myMap.ChooseThree, myMap.ChooseFour };
            temp = temp.Where(s => !string.IsNullOrEmpty(s)).ToArray();

            //split the details into seperate arrays
            string[] spriteLevels = temp.Select(s => Conventions.GetChoiceElements(s)["level"]).ToArray();
            string[] spriteNames = temp.Select(s => Conventions.GetChoiceElements(s)["name"]).ToArray();
            string[] spriteTypes = temp.Select(s => Conventions.GetChoiceElements(s)["type"]).ToArray();

            //add in health if type is monster
            string[] spriteHealths = new string[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                if (Conventions.GetChoiceElements(temp[i])["type"] == "monster")
                    spriteHealths[i] = Conventions.GetChoiceElements(temp[i])["health"];
                else
                    spriteHealths[i] = "-1";
            }

            //find caves and replace their names in the image
            for (int i = 0; i < spriteNames.Count(); i++)
            {
                if (spriteImages[i].Contains("cave"))
                {
                    spriteNames[i] = $"Cave";
                    spriteTypes[i] = $"cave";
                }
            }

            //combine the string arrays
            string[,] spriteDetails = new string[spriteImages.Length, 5];
            for (int i = 0; i < spriteImages.Length; i++)
            {
                spriteDetails[i, 0] = spriteImages[i];
                spriteDetails[i, 1] = spriteNames[i];
                spriteDetails[i, 2] = spriteHealths[i];
                spriteDetails[i, 3] = spriteLevels[i];
                spriteDetails[i, 4] = spriteTypes[i];
            }

            return spriteDetails;
        }

        private static Image<Rgba32> ConvertToTile(this Image<Rgba32> image, Size size)
        {
            Image<Rgba32> result = image.Clone(
                ctx => ctx.Resize(
                    new ResizeOptions
                    {
                        Size = size,
                        Mode = ResizeMode.Crop
                    }));

            return result;
        }

        private static Image<Rgba32> PlaceHero(Image<Rgba32> image, Image<Rgba32> heroSpr, int statsSpriteWidth, int padding)
        {
            GraphicsOptions options = new GraphicsOptions();
            options.BlenderMode = PixelBlenderMode.Atop;

            Image<Rgba32> img = image;
            Image<Rgba32> heroSprite = heroSpr;

            heroSprite = heroSprite.ConvertToTile(new Size(Math.Min(250, heroSprite.Width), heroSprite.Height - Math.Max(0, heroSprite.Width - 250)));

            img = img.Clone(ctx => ctx.DrawImage(options, heroSprite, new Point { X = (statsSpriteWidth + padding) + 10, Y = img.Height - padding - heroSprite.Height }));

            return img;
        }

        private static Image<Rgba32> PlaceHeroStats(Image<Rgba32> image, Image<Rgba32> panelImage, Font font, string guildId)
        {
            Image<Rgba32> img = image;
            Image<Rgba32> statsPanel = panelImage;

            GraphicsOptions options = new GraphicsOptions();
            options.BlenderMode = PixelBlenderMode.Atop;

            string stats = HeroFunctions.GetHeroStats(guildId);

            statsPanel = statsPanel.Clone(ctx => ctx.AddTextWithWrap(font, $"{HeroFunctions.GetHeroStatsForImage(guildId).Item1}", Rgba32.White, 20, 20));
            statsPanel = statsPanel.Clone(ctx => ctx.AddTextWithWrap(font, $"{HeroFunctions.GetHeroStatsForImage(guildId).Item2}\n\n" +
                                                                           $"{InventoryFunctions.GetInventoryStats(guildId)}", Rgba32.White, 20, "left"));

            img = img.Clone(ctx => ctx.DrawImage(options, statsPanel, new Point { X = 1, Y = image.Height - statsPanel.Height }));

            return img;
        }

        private static Image<Rgba32> PlaceSprites(Image<Rgba32> image, Font font, string[,] spriteInfo, int[] heroInfo, string fontType, int padding)
        {
            Image<Rgba32> img = image;

            GraphicsOptions options = new GraphicsOptions();
            options.BlenderMode = PixelBlenderMode.Atop;

            int numberOfSprites = spriteInfo.GetLength(0);
            int MaxSpriteWidth = (img.Width - padding * (numberOfSprites + 1)) / numberOfSprites;
            int MaxSpriteHeight = (img.Height - padding * (2 + 1)) / 2; //"2" is for the two rows of items (one for choices and one for middle)

            //place sprites
            for (int i = 0; i < numberOfSprites; i++)
            {
                //resize sprite to fit properly
                var choice = Image.Load($"{ spriteInfo[i, 0] }");
                choice = ResizeChoiceSprite(choice, MaxSpriteHeight, MaxSpriteWidth);

                //properly place the images in the background
                int x = ((img.Width / numberOfSprites) * (2 * i + 1) - choice.Width) / 2;
                img = img.Clone(ctx => ctx.DrawImage(options, choice, new Point { X = x, Y = padding }));

                //place panel image for choice names
                Image<Rgba32> choicesPanel = Image.Load(@"Other/choicensmall.png");
                img = CreateChoiceLabel(img, choicesPanel, font, heroInfo, spriteInfo, x + choice.Width / 2, i);
            }

            return img;

        }

        private static Image<Rgba32> ResizeChoiceSprite(Image<Rgba32> choice, int MaxSpriteHeight, int MaxSpriteWidth)
        {
            Image<Rgba32> result = choice;

            if (choice.Height > MaxSpriteHeight)
                result = result.ConvertToTile(new Size(choice.Width * MaxSpriteHeight / choice.Height, MaxSpriteHeight));

            if (choice.Width > MaxSpriteWidth)
                result = result.ConvertToTile(new Size(MaxSpriteWidth, choice.Height * MaxSpriteWidth / choice.Width));

            return result;
        }

        private static Image<Rgba32> CreateChoiceLabel(Image<Rgba32> image, Image<Rgba32> labelImage, Font font, int[] heroInfo, string[,] spriteInfo, int choiceCenterX, int choiceNumber)
        {
            Image<Rgba32> img = image;
            Image<Rgba32> choicesPanel = labelImage;

            int wpLevel = heroInfo[0];
            int arLevel = heroInfo[1];

            int spriteLevel = Int32.Parse(spriteInfo[choiceNumber, 3]);
            int spriteCombLevel = Progression.GetNextWeaponLevel(spriteLevel) + Progression.GetNextArmorLevel(spriteLevel);

            string spriteName = spriteInfo[choiceNumber, 1];
            string spriteType = spriteInfo[choiceNumber, 4];
            string healthString = (spriteInfo[choiceNumber, 2] == "-1" || spriteType == "cave") 
                                  ? "" : $"Health:{spriteInfo[choiceNumber, 2]}";

            GraphicsOptions options = new GraphicsOptions();
            options.BlenderMode = PixelBlenderMode.Atop;

            if (((spriteType == "monster" || spriteType == "cave") && spriteCombLevel < wpLevel + arLevel - 1) ||
                (spriteType == "weapon" && spriteLevel < wpLevel - 1) ||
                (spriteType == "armor" && spriteLevel < arLevel - 1))
            {
                choicesPanel = choicesPanel.Clone(ctx => ctx.AddTextWithWrap(font, $"{spriteName} ({spriteLevel})\n" +
                                                                                   $"{healthString}", Rgba32.LimeGreen, 2, "center"));
            }
            else if (((spriteType == "monster" || spriteType == "cave") && spriteCombLevel > wpLevel + arLevel + 1) ||
                (spriteType == "weapon" && spriteLevel > wpLevel + 1) ||
                (spriteType == "armor" && spriteLevel > arLevel + 1))
            {
                choicesPanel = choicesPanel.Clone(ctx => ctx.AddTextWithWrap(font, $"{spriteName} ({spriteLevel})\n" +
                                                                                   $"{healthString}", Rgba32.Red, 2, "center"));
            }
            else
            {
                choicesPanel = choicesPanel.Clone(ctx => ctx.AddTextWithWrap(font, $"{spriteName} ({spriteLevel})\n" +
                                                                                   $"{healthString}", Rgba32.LightBlue, 2, "center"));
            }
            img = img.Clone(ctx => ctx.DrawImage(options, choicesPanel, new Point { X = choiceCenterX - choicesPanel.Width / 2, Y = 10 }));

            return img;
        }

        private static IImageProcessingContext<TPixel> AddText<TPixel>(this IImageProcessingContext<TPixel> processingContext, Font font, string text, TPixel color, Point myPoint)
          where TPixel : struct, IPixel<TPixel>
        {
            return processingContext.Apply(img =>
            {
                img.Mutate(i => i.DrawText(text, font, color, new PointF(myPoint.X, myPoint.Y)));
            });
        }

        private static IImageProcessingContext<TPixel> AddTextWithWrap<TPixel>(this IImageProcessingContext<TPixel> processingContext, Font font, string text, TPixel color, float padding, string align)
            where TPixel : struct, IPixel<TPixel>
        {
            return processingContext.Apply(img =>
            {
                float targetWidth = img.Width - padding;
                float targetHeight = img.Height - padding;

                var scaledFont = font;
                SizeF s = new SizeF(float.MaxValue, float.MaxValue);

                while (s.Height > targetHeight)
                {
                    scaledFont = new Font(scaledFont, scaledFont.Size - 1);
                    s = TextMeasurer.Measure(text, new RendererOptions(scaledFont)
                    {
                        WrappingWidth = targetWidth
                    });
                }

                var center = new PointF(padding, img.Height / 2 - 2);
                var grOptions = new TextGraphicsOptions(true)
                {
                    HorizontalAlignment = (align == "center") ? HorizontalAlignment.Center : HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center,
                    WrapTextWidth = targetWidth
                };
                img.Mutate(i => i.DrawText(grOptions, text, scaledFont, color, center));
            });
        }

        private static IImageProcessingContext<TPixel> AddTextWithWrap<TPixel>(this IImageProcessingContext<TPixel> processingContext, Font font, string text, TPixel color, float padding, int yaxis)
            where TPixel : struct, IPixel<TPixel>
        {
            return processingContext.Apply(img =>
            {
                float targetWidth = img.Width - padding;
                float targetHeight = img.Height - padding;

                var scaledFont = font;
                SizeF s = new SizeF(float.MaxValue, float.MaxValue);

                while (s.Height > targetHeight)
                {
                    scaledFont = new Font(scaledFont, scaledFont.Size - 1);
                    s = TextMeasurer.Measure(text, new RendererOptions(scaledFont)
                    {
                        WrappingWidth = targetWidth
                    });
                }

                var grOptions = new TextGraphicsOptions(true)
                {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    WrapTextWidth = targetWidth
                };
                img.Mutate(i => i.DrawText(grOptions, text, scaledFont, color, new Point { X = (int)padding, Y = yaxis }));
            });
        }
    }
}

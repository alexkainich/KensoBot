﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Kensoby;
using Kensoby.Modules;

namespace Kensoby.Modules
{
    public class Common : ModuleBase<SocketCommandContext>
    {
        public static Tuple<int, string> GuildOrUser(SocketCommandContext context)
        {
            int guildOrUser;
            string guildOrUserId = "";
            try
            {
                guildOrUserId = context.Guild.Id.ToString();
                guildOrUser = 1;
            }
            catch
            {
                guildOrUserId = context.User.Username.ToString() + "#" + context.User.DiscriminatorValue.ToString();
                guildOrUser = 2;
            }
            return Tuple.Create(guildOrUser, guildOrUserId);
        }

        //.png names to be used in map generation
        public static string GuildOrUserMap(SocketCommandContext context)
        {
            string guildOrUserId = "";
            try
            {
                guildOrUserId = context.Guild.Id.ToString();
            }
            catch
            {
                guildOrUserId = context.User.Id.ToString();
            }
            return guildOrUserId;
        }

        public static string GuildOrUserName(SocketCommandContext context)
        {
            string guildOrUserName = "";
            try
            {
                guildOrUserName = context.Guild.Name.ToString();
            }
            catch
            {
                guildOrUserName = context.User.Username.ToString() + "#" + context.User.DiscriminatorValue.ToString();
            }
            return guildOrUserName;
        }

        public static string[] GetUsersInGuild(SocketCommandContext context)
        {
            try
            {
                List<Discord.WebSocket.SocketGuildUser> users = context.Guild.Users.ToList();
                string[] userIds = new string[users.Capacity];

                for (int i = 0; i < users.Count(); i++)
                    userIds[i] = users[i].Username.ToString() + "#" + users[i].DiscriminatorValue.ToString();

                return userIds;
            }
            catch
            {
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Kensoby;

namespace Kensoby.Modules
{
    public class Drop : Common
    {
        [Command("drop")]
        public async Task SwapItem()
        {
            string guildId = GuildOrUser(Context).Item2;;

            string change = InventoryFunctions.DropTempItem(guildId);

            if (change == "none")
                await ReplyAsync($"No item to drop!\nPlease use '/k play'");
            else
                await ReplyAsync($"You dropped the {Conventions.GetChoiceElements(change)["name"]}");
        }
    }
}
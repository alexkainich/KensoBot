﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Kensoby;

namespace Kensoby.Modules
{
    public class Help : Common
    {
        [Command("help")]
        public async Task Game()
        {
            await ReplyAsync($"---   Welcome to Kensoby !  ---\n" +
                             $"\n" +
                             $"Function: Command\n" +
                             $"\n" +
                             $"Load Map:    /k play\n" +
                             $"Load Map of level #:     /k play #\n" +
                             $"   First Choice:     /k move 1\n" +
                             $"   Second Choice:    /k move 2\n" +
                             $"   Third Choice:     /k move 3\n" +
                             $"   Keep Item:    /k keep\n" +
                             $"   Drop Item:    /k drop\n" +
                             $"\n" +
                             $"Open Inventory:      /k inv\n" +
                             $"   Equip Weapon:     /k inv weapon\n" +
                             $"   Equip Armor:      /k inv armor\n" +
                             $"\n" +
                             $"Player VS Player:    /k pvp\n" +
                             $"   Accept PvP Invite:    /k pvp accept\n" +
                             $"   Reject PvP Invite:    /k pvp reject\n" +
                             $"\n" +
                             $"Hero Stats:      /k stats\n" +
                             $"Top Heroes by level:     /k top\n");
        }
    }
}
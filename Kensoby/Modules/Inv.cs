﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Kensoby;

namespace Kensoby.Modules
{
    [Group("inv")]
    public class Inv : Common
    {
        [Command("")]
        public async Task Game()
        {
            string guildId = GuildOrUser(Context).Item2;;

            string invstats = InventoryFunctions.GetInventoryStats(guildId);

            if (invstats == null)
                await ReplyAsync("Please create a new Hero by using '/k play'");
            else
                await ReplyAsync($"{invstats}");
        }

        [Command("weapon")]
        public async Task ChangeWeapon()
        {
            string guildId = GuildOrUser(Context).Item2;;

            Tuple<string, string> change = InventoryFunctions.ChangeItem(1, guildId);

            if (change.Item2 == "none")
                await ReplyAsync("No item to change!");
            else
                await ReplyAsync($"You equipped the {change.Item2} and stored the {change.Item1}!");
        }

        [Command("armor")]
        public async Task ChangeArmor()
        {
            string guildId = GuildOrUser(Context).Item2;;

            Tuple<string, string> change = InventoryFunctions.ChangeItem(2, guildId);

            if (change.Item2 == "none")
                await ReplyAsync("No item to change!");
            else
                await ReplyAsync($"You equipped the {change.Item2} and stored the {change.Item1}!");
        }
    }
}
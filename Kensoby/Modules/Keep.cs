﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Kensoby;

namespace Kensoby.Modules
{
    public class Keep : Common
    {
        [Command("keep")]
        public async Task SwapItem()
        {
            string guildId = GuildOrUser(Context).Item2;;

            Tuple<string,string> change = InventoryFunctions.ReplaceInventoryItem(guildId);

            if (change.Item2 == "none")
                await ReplyAsync($"No item to replace!\nPlease use '/k play'");
            else
                await ReplyAsync($"You got the {change.Item2} and dropped {change.Item1}!");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Graphics;
using Kensoby.Modules;
using Fighting;

namespace KensoBot.Modules
{
    [Group("Move")]
    public class Move : Common
    {
        [Command("")]
        public async Task Game(string myMove = "none")
        {
            string guildId = GuildOrUser(Context).Item2;

            if (myMove == "none")
            {
                await ReplyAsync("Please use /k move 1,2,3 to choose an action");
            }
            else if (MapFunctions.GuildMapLoaded(guildId))
            {
                if (myMove == "1")
                {
                    await OneAsync();
                }
                else if (myMove == "2")
                {
                    await TwoAsync();
                }
                else if (myMove == "3")
                {
                    await ThreeAsync();
                }
                else if (myMove == "4")
                {
                    await FourAsync();
                }
                else
                {
                    await ReplyAsync("Please use /k move 1,2,3 to choose an action");
                }
            }
            else
            {
                await ReplyAsync("Please use '/k play' or '/k play level:#'");
            }
        }

        public async Task OneAsync()
        {
            string guildId = GuildOrUser(Context).Item2;

            string result = chooseAction(1, guildId);
            await ReplyAsync($"{result}");

        }

        public async Task TwoAsync()
        {
            string guildId = GuildOrUser(Context).Item2;

            string result = chooseAction(2, guildId);
            await ReplyAsync($"{result}");
        }

        public async Task ThreeAsync()
        {
            string guildId = GuildOrUser(Context).Item2;

            string result = chooseAction(3, guildId);
            await ReplyAsync($"{result}");
        }

        public async Task FourAsync()
        {
            await ReplyAsync($"Why would you need this?");
        }

        public static string chooseAction(int choice, string guildId)
        {
            Map myMap;
            Hero myHero;
            using (var db = new KensobyContext())
            {
                myMap = MapFunctions.GetMap(db, guildId);
                myHero = HeroFunctions.GetHero(db, guildId);
            }

            //Item1: sprite, Item2: stats
            Tuple<string, string> myChoice = GetChoiceStrings(choice, myMap);

            //conventions
            Dictionary<string, string> choiceElements = Conventions.GetChoiceElements(myChoice.Item2);
            Dictionary<string, string> heroElements = Conventions.GetHeroElements(myHero);

            //is choice a monster a weapon or an armor ? (looks behind caves)
            string reply = "";
            if (choiceElements["type"] == "monster")
            {
                reply = ChoiceMonsterReply(guildId, myChoice.Item1.Contains("cave"), heroElements, choiceElements);
            }
            else if (choiceElements["type"] == "weapon")
            {
                reply = ChoiceWeaponReply(guildId, myChoice);
            }
            else if (choiceElements["type"] == "armor")
            {
                reply = ChoiceArmorReply(guildId, myChoice);
            }

            return reply;
        }

        private static Tuple<string, string> GetChoiceStrings(int choice, Map myMap)
        {
            string myChoiceStats = "";
            string myChoiceSprite = "";
            switch (choice)
            {
                case 1:
                    myChoiceStats = myMap.ChooseOne;
                    myChoiceSprite = myMap.SpriteOne;
                    break;
                case 2:
                    myChoiceStats = myMap.ChooseTwo;
                    myChoiceSprite = myMap.SpriteTwo;
                    break;
                case 3:
                    myChoiceStats = myMap.ChooseThree;
                    myChoiceSprite = myMap.SpriteThree;
                    break;
                case 4:
                    myChoiceStats = myMap.ChooseFour;
                    myChoiceSprite = myMap.SpriteFour;
                    break;
            }

            return Tuple.Create(myChoiceSprite, myChoiceStats);
        }

        private static string ChoiceMonsterReply(string guildId, bool cave, Dictionary<string, string> heroElements, Dictionary<string, string> choiceElements)
        {
            Tuple<int, int> healths = Fight.PveFight(heroElements, choiceElements);
            int heroHealth = healths.Item1;
            int heroLevel = Int32.Parse(heroElements["level"]);
            int monsterLevel = Int32.Parse(choiceElements["level"]);
            int monsterExp = Int32.Parse(choiceElements["exp"]);
            int levelDifference = monsterLevel - heroLevel;

            string lostItem = HeroFunctions.UpdateHeroAfterPvE(guildId, heroHealth.ToString(), monsterExp, levelDifference);
            MapFunctions.MapCompleted(guildId, "Empty");

            string reply;

            if (heroHealth == 0)
            {
                if (cave)
                {
                    if (lostItem == "none")
                        reply = $"You found a {choiceElements["name"]} and died in the fight. \n" +
                                "You cheat death and come back to life!";
                    else
                        reply = $"You found a {choiceElements["name"]} and died in the fight. \n" +
                                "You cheat death and come back to life! \n" +
                                "However, something is missing ...";
                }
                else
                {
                    if (lostItem == "none")
                        reply = "Hero died! \n" +
                                "You cheat death and come back to life";
                    else
                        reply = "Hero died! \n" +
                                "You cheat death and come back to life \n" +
                                "However, something is missing ...";
                }
            }
            else
            {
                reply = $"You win the {choiceElements["name"]} and your life drops to {heroHealth}";
            }

            return reply;
        }

        private static string ChoiceWeaponReply(string guildId, Tuple<string, string> myChoice)
        {
            InventoryFunctions.UpdateHeroAfterWeapon(guildId, myChoice.Item2);
            MapFunctions.MapCompleted(guildId, "Confirm");
            Tuple<string, string> weaponTemp = InventoryFunctions.WeaponPending(guildId);
            if (weaponTemp.Item2 != "none")
            {
                return $"Do you want to keep the {Conventions.GetChoiceElements(weaponTemp.Item2)["name"]} and drop the {Conventions.GetChoiceElements(weaponTemp.Item1)["name"]}? \n" +
                         "Please use '/k keep' or '/k drop'";
            }
            else
            {
                return $"You got the {Conventions.GetChoiceElements(weaponTemp.Item1)["name"]} !";
            }
        }

        private static string ChoiceArmorReply(string guildId, Tuple<string, string> myChoice)
        {
            InventoryFunctions.UpdateHeroAfterArmor(guildId, myChoice.Item2);
            MapFunctions.MapCompleted(guildId, "Confirm");
            Tuple<string, string> armorTemp = InventoryFunctions.ArmorPending(guildId);
            if (armorTemp.Item2 != "none")
            {
                return $"Do you want to keep the {Conventions.GetChoiceElements(armorTemp.Item2)["name"]} and drop the {Conventions.GetChoiceElements(armorTemp.Item1)["name"]}? \n" +
                         "Please use '/k keep' or '/k drop'";
            }
            else
            {
                return $"You got the {Conventions.GetChoiceElements(armorTemp.Item1)["name"]} !";
            }
        }
    }
}

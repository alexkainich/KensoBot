﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Graphics;
using Kensoby.Modules;
using Fighting;

namespace KensoBot.Modules
{
    [Group("play")]
    public class Play : Common
    {
        [Command("")]
        public async Task Game(string mapLevel = "none")
        {
            string userIdMap = GuildOrUserMap(Context);
            string guildId = GuildOrUser(Context).Item2;
            int guildOrUser = GuildOrUser(Context).Item1;
            string mapname = "";

            //check which method to execute (choice or change map level)
            string myLevel = await ChooseCommand(mapLevel, guildId);

            //return if nothing more to do here
            if (myLevel == "end")
                return;

            //check if id is new
            if (HeroFunctions.IsGuildNew(guildId) == true)
            {
                string guildName = GuildOrUserName(Context);
                HeroFunctions.CreateNewHero(guildId, guildName);
                MapFunctions.CreateVeryFirstMap(guildId);
                mapname = GenerateMap.Generate(guildId, userIdMap);
            }
            //if not, then check for pending item (to drop or keep)
            else
            {
                string itemPending = InventoryFunctions.ItemPending(guildId);
                Tuple<string, string, string> pvpPending = PvpFunctions.IsPvpMessagePending(guildId);

                if (itemPending != "none")
                {
                    await Context.Channel.SendMessageAsync($"Do you want to keep the {Conventions.GetChoiceElements(itemPending)["name"]} ? \n" +
                                                            "Please use '/k keep' or '/k drop'");
                    return;
                }
                //If no items pending, then check if map is already loaded (but not played, by /k move 1,2,3)
                else if (MapFunctions.GuildMapLoaded(guildId) == true)
                {
                    mapname = GenerateMap.Generate(guildId, userIdMap);
                }
                //If a new map will be created, then first check if there is any pvp challenge is pending
                else
                {
                    switch (pvpPending.Item1)
                    {
                        case "challenged":
                            await Context.Channel.SendMessageAsync($"You are waiting for {pvpPending.Item2} to accept the PvP !\n" +
                                                           $"Use '/k pvp reject' if you want to cancel the PvP");
                            return;
                        case "pending":
                            await Context.Channel.SendMessageAsync($"Player {pvpPending.Item2}, level {pvpPending.Item3} has challenged you to a pvp !\n" +
                                                           $"Please use /k pvp accept' or '/k pvp reject'");
                            return;
                        case "won":
                            await Context.Channel.SendMessageAsync($"You have won {pvpPending.Item2} ! Well done !");
                            return;
                        case "lost":
                            await Context.Channel.SendMessageAsync($"You have lost to {pvpPending.Item2} !\n" +
                                                                   "You cheat death and come back to life");
                            return;
                    }
                }
            }

            //if you are still here, then generate a new map !
            if (mapname == "")
            {
                if (myLevel != "none")
                {
                    Hero myHero = HeroFunctions.GetHero(new KensobyContext(), guildId);
                    myLevel = (Int32.Parse(myLevel) < Int32.Parse(myHero.Level)) ? myLevel : myLevel = "none";
                }

                MapFunctions.CreateNewMap(new KensobyContext(), guildId, myLevel);
                mapname = GenerateMap.Generate(guildId, userIdMap);
            }

            //and send it to discord
            var eb = new EmbedBuilder();
            eb.WithImageUrl(@"http://35.204.137.71/kensobot/" + mapname + ".png");

            if (guildOrUser == 1)
                await Context.Channel.SendMessageAsync("", false, eb);
            else
                await Context.User.SendMessageAsync("", false, eb);
        }

        //should return either "none", or "end" or a level
        public async Task<string> ChooseCommand(string mapLevel, string guildId)
        {
            int myLevel = -1;
            string result = "none";

            if (mapLevel == "none")
            {
                result = "none";
            }
            else if (MapFunctions.GuildMapLoaded(guildId))
            {
                await ReplyAsync("Please use /k move 1,2,3 to choose an action");
                result = "end";
            }
            else if (mapLevel != "none")
            {
                if (int.TryParse(mapLevel, out myLevel))
                {
                    if (myLevel >= 1)
                        result = myLevel.ToString();
                    else
                    {
                        await ReplyAsync("Level must be higher than 0");
                        result = "end";
                    }
                }
                else
                {
                    await ReplyAsync("You can use '/k play #' to specify a map level");
                    result = "end";
                }
            }
            else
            {
                await ReplyAsync("Please use '/k play' or '/k play #'");
                result = "end";
            }

            return result;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using KensobyEF;
using KensobyEF.Model;
using Fighting;

namespace Kensoby.Modules
{
    [Group("pvp")]
    public class PvP : Common
    {
        [Command("")]
        public async Task PlayerVsPlayer(string guildIdToChallenge = "")
        {
            //accept challenge
            if (guildIdToChallenge == "accept")
            {
                await AcceptChallenge();
            }
            //reject challenge
            else if (guildIdToChallenge == "reject")
            {
                await RejectChallenge();
            }
            else
            {
                //if pvp is pending
                string guildId = GuildOrUser(Context).Item2;
                Tuple<string, string, string> pvpPending = PvpFunctions.IsPvpMessagePending(guildId);
                switch (pvpPending.Item1)
                {
                    case "challenged":
                        await Context.Channel.SendMessageAsync($"You are waiting for {pvpPending.Item2} to accept the PvP !\n" +
                                                       $"Use '/k pvp reject' if you want to cancel the PvP");
                        return;
                    case "pending":
                        await Context.Channel.SendMessageAsync($"Player {pvpPending.Item2}, level {pvpPending.Item3} has challenged you to a pvp !\n" +
                                                       $"Please use /k pvp accept' or '/k pvp reject'");
                        return;
                    case "won":
                        await Context.Channel.SendMessageAsync($"You have won {pvpPending.Item2} ! Well done !");
                        return;
                    case "lost":
                        await Context.Channel.SendMessageAsync($"You have lost to {pvpPending.Item2} !\n" +
                                       "You cheat death and come back to life");
                        return;
                }

                //if pvp was entered without arguments
                if (guildIdToChallenge == "")
                {
                    string reply = PvpFunctions.GetPvpStats(guildId);
                    reply = reply + "\n\n" + "Use '/k pvp username#tag' to challenger a user to a pvp";

                    await ReplyAsync(reply);

                    return;
                }

                //if you are still here then challenge player
                Hero heroChallenger = HeroFunctions.GetHero(new KensobyContext(), guildIdToChallenge);
                string result = PvpFunctions.ChallengePlayer(guildId, guildIdToChallenge);
                switch (result)
                {
                    case "done":
                        await ReplyAsync($"Invitation to {heroChallenger.Name} for pvp sent!");
                        break;
                    case "same":
                        await ReplyAsync("You have already challenged this player\n" +
                                     "Wait till he accepts, or use '/k pvp reject' to cancel the pvp");
                        break;
                    case "other":
                        await ReplyAsync("This player has already been challenged !\n" +
                                         "Please wait until their PvP is completed");
                        break;
                    case "none":
                        await ReplyAsync("Wrong user name id. Invitation not sent");
                        break;
                    case "pending":
                        await ReplyAsync("You have been challenged by another player !\n" +
                                     "Please use '/k pvp accept' or '/k pvp reject'");
                        break;
                    case "self":
                        await ReplyAsync("Trying to kill yourself is not allowed");
                        break;
                    default:
                        await ReplyAsync("You have already challenged another player !\n" +
                                    "If you want to start a new pvp use '/k pvp reject'\n" +
                                    "Then '/k pvp username#tag' to challenge a new player");
                        break;
                }
            }
        }

        public async Task AcceptChallenge()
        {
            string guildId = GuildOrUser(Context).Item2;
            string guildIdChallenger = PvpFunctions.AcceptPvp(guildId);

            if (guildIdChallenger == "none")
                await ReplyAsync($"There are no pending PvPs to accept");
            else
            {
                Hero hero, heroChallenger;
                using (var db = new KensobyContext())
                {
                    hero = HeroFunctions.GetHero(db, guildId);
                    heroChallenger = HeroFunctions.GetHero(db, guildIdChallenger);
                }

                Dictionary<string, string> heroElements = Conventions.GetHeroElements(hero);
                Dictionary<string, string> heroElementsChallenger = Conventions.GetHeroElements(heroChallenger);

                Tuple<int, int> result = Fight.PvpFight(heroElements, heroElementsChallenger);

                //if our hero won the fight
                if (result.Item2 == 0)
                {
                    hero = HeroFunctions.UpdateHeroAfterPvP(guildId, result.Item1.ToString());
                    heroChallenger = HeroFunctions.HeroPvpDead(guildIdChallenger);
                    PvpFunctions.UpdatePvP(guildId, "won");
                    PvpFunctions.UpdatePvPChallenger(guildIdChallenger, "lost");

                    await ReplyAsync($"You have won {heroChallenger.Name} ! Well done !\n");
                }
                //if our hero lost the fight
                else
                {
                    heroChallenger = HeroFunctions.UpdateHeroAfterPvP(guildIdChallenger, result.Item2.ToString());
                    hero = HeroFunctions.HeroPvpDead(guildId);
                    PvpFunctions.UpdatePvP(guildId, "lost");
                    PvpFunctions.UpdatePvPChallenger(guildIdChallenger, "won");

                    await ReplyAsync($"You have lost to {heroChallenger.Name} !\n" +
                                      "You cheat death and come back to life!");
                }
            }
        }

        public async Task RejectChallenge()
        {
            string guildId = GuildOrUser(Context).Item2;
            string guildIdChallenger = PvpFunctions.RejectPvp(guildId);

            if (guildIdChallenger == "none")
                await ReplyAsync($"There are no pending PvPs to reject");
            else
            {
                await ReplyAsync($"You have no more pending PvPs");
            }
        }
    }
}

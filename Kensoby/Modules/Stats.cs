﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Kensoby;

namespace Kensoby.Modules
{
    public class Stats : Common
    {
        [Command("stats")]
        public async Task Game()
        {
            string guildId = GuildOrUser(Context).Item2;;

            string stats = HeroFunctions.GetHeroStats(guildId);

            await ReplyAsync($"{stats}");
        }
    }
}
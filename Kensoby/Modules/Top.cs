﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using KensobyEF;
using KensobyEF.Model;
using Fighting;

namespace Kensoby.Modules
{
    [Group("top")]
    public class Top : Common
    {
        [Command("")]
        public async Task PlayerVsPlayer()
        {
            string[] guildUsers = GetUsersInGuild(Context);

            string reply = "";
            if (guildUsers == null)
                //get top heroes by won pvps
                reply = HeroFunctions.GetTopHeroes();
            else
                //get top heroes by level
                reply = HeroFunctions.GetTopHeroes(guildUsers);

            await ReplyAsync(reply);

            return;

        }
    }
}
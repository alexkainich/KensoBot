﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using KensobyEF;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.IO;


namespace KensoBot
{
    class Program
    {
        static void Main(string[] args) => new Program().RunBotAsync().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        public async Task RunBotAsync()
        {
            //Remove on production
            restartDatabase();
            //Keeps regenerating Heroes health every 30min. Heroes go to full health in 24hours.
            UpdateHealthAsync(30, 24, new CancellationToken());
            //Keeps deleting the 2minute-old .png files from the Map/ folder every 2 minutes.
            DeleteOldMapsAsync(2, 2, new CancellationToken());

            _client = new DiscordSocketClient();
            _commands = new CommandService();
            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            //https://discordapp.com/developers/applications/me
            string botToken = "NDM0MjkwNjg3NzY5MzEzMjkx.DbIROQ.FXEBAmU9qn5r81jB3CTHoZ1U-XU";

            //event subscriptions
            _client.Log += Log;
            //_client.UserJoined += AnnounceUserJoined;

            await RegisterCommandsAsync();
            await _client.LoginAsync(Discord.TokenType.Bot, botToken);
            await _client.StartAsync();
            await Task.Delay(-1);
        }

        //private async Task AnnounceUserJoined(SocketGuildUser user)
        //{
        //    var guild = user.Guild;
        //    var channel = guild.DefaultChannel;
        //    await channel.SendMessageAsync($"Welcome, {user.Mention}");
        //}

        private Task Log(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            _client.MessageReceived += HandleCommandAsync;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            if (message is null || message.Author.IsBot) return;
            int argPos = 0;
            if (message.HasStringPrefix("/k ", ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                var context = new SocketCommandContext(_client, message);
                var result = await _commands.ExecuteAsync(context, argPos, _services);

                if (!result.IsSuccess)
                    Console.WriteLine(result.ErrorReason);

            }
        }

        public void restartDatabase() //Remove on production
        {
            using (var db = new KensobyContext())
            {
                db.Database.EnsureDeleted();
                db.Database.Migrate();
            }
        }

        public async Task UpdateHealthAsync(int interval, int hoursTime, CancellationToken cancellationToken)
        {
            while (true)
            {
                await HeroFunctions.UpdateHealth(interval, hoursTime);
                await Task.Delay(TimeSpan.FromMinutes(interval), cancellationToken);
            }
        }

        public async Task DeleteOldMapsAsync(int interval, int minutesTime, CancellationToken cancellationToken)
        {
            while (true)
            {
                var files = new DirectoryInfo(@"Map/").GetFiles("*.png");
                foreach (var file in files)
                {
                    if (DateTime.UtcNow - file.CreationTimeUtc > TimeSpan.FromMinutes(minutesTime))
                    {
                        File.Delete(file.FullName);
                    }
                }
                await Task.Delay(TimeSpan.FromMinutes(interval), cancellationToken);
            }
        }
    }
}

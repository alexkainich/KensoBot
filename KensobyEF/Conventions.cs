﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KensobyEF;
using KensobyEF.Model;
using System.Linq;
using Randomization;

namespace KensobyEF
{
    public class Conventions
    {
        ///// Conventions /////
        public static Dictionary<string, string> GetChoiceElements(string myChoice)
        {
            string[] substrings = myChoice.Split(',');
            string chType = null;
            string chExp = null;
            string chLevel = null;
            string chName = null;
            string chHealth = null;
            string chMana = null;
            string chDmg = null;
            string chArmor = null;
            string chLifes = null;

            for (int i = 0; i < substrings.Count(); i++)
            {
                switch (substrings[i].Split(':')[0])
                {
                    case "type":
                        chType = substrings[i].Split(':')[1];
                        break;
                    case "exp":
                        chExp = substrings[i].Split(':')[1];
                        break;
                    case "level":
                        chLevel = substrings[i].Split(':')[1];
                        break;
                    case "name":
                        chName = substrings[i].Split(':')[1];
                        break;
                    case "health":
                        chHealth = substrings[i].Split(':')[1];
                        break;
                    case "mana":
                        chMana = substrings[i].Split(':')[1];
                        break;
                    case "dmg":
                        chDmg = substrings[i].Split(':')[1];
                        break;
                    case "ac":
                        chArmor = substrings[i].Split(':')[1];
                        break;
                    case "lifes":
                        chLifes = substrings[i].Split(':')[1];
                        break;
                }
            }

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("type", chType);
            dictionary.Add("exp", chExp);
            dictionary.Add("level", chLevel);
            dictionary.Add("name", chName);
            dictionary.Add("health", chHealth);
            dictionary.Add("mana", chMana);
            dictionary.Add("dmg", chDmg);
            dictionary.Add("ac", chArmor);
            dictionary.Add("lifes", chLifes);

            return dictionary;
        }

        public static Dictionary<string, string> GetHeroElements(Hero myHero)
        {
            string[] WeaponSubstrings = myHero.Weapon.Split(',');
            string[] ArmorSubstrings = myHero.Armor.Split(',');

            string level = myHero.Level;
            string health = myHero.Health;
            string maxhealth = myHero.MaxHealth;
            string mana = myHero.Mana;
            string maxmana = myHero.MaxMana;
            string exp = myHero.Exp;
            string wpType = null;
            string wpLevel = null;
            string wpName = null;
            string wpDmg = null;
            string wpLifes = null;
            string arType = null;
            string arLevel = null;
            string arName = null;
            string arDmg = null;
            string arArmor = null;

            for (int i = 0; i < WeaponSubstrings.Count(); i++)
            {
                switch (WeaponSubstrings[i].Split(':')[0])
                {
                    case "type":
                        wpType = WeaponSubstrings[i].Split(':')[1];
                        break;
                    case "level":
                        wpLevel = WeaponSubstrings[i].Split(':')[1];
                        break;
                    case "name":
                        wpName = WeaponSubstrings[i].Split(':')[1];
                        break;
                    case "dmg":
                        wpDmg = WeaponSubstrings[i].Split(':')[1];
                        break;
                    case "lifes":
                        wpLifes = WeaponSubstrings[i].Split(':')[1];
                        break;
                }
            }

            for (int i = 0; i < ArmorSubstrings.Count(); i++)
            {
                switch (ArmorSubstrings[i].Split(':')[0])
                {
                    case "type":
                        arType = ArmorSubstrings[i].Split(':')[1];
                        break;
                    case "level":
                        arLevel = ArmorSubstrings[i].Split(':')[1];
                        break;
                    case "name":
                        arName = ArmorSubstrings[i].Split(':')[1];
                        break;
                    case "dmg":
                        arDmg = ArmorSubstrings[i].Split(':')[1];
                        break;
                    case "ac":
                        arArmor = ArmorSubstrings[i].Split(':')[1];
                        break;
                }
            }

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("level", level);
            dictionary.Add("health", health);
            dictionary.Add("maxhealth", maxhealth);
            dictionary.Add("mana", mana);
            dictionary.Add("maxmana", maxmana);
            dictionary.Add("exp", exp);
            dictionary.Add("wpType", wpType);
            dictionary.Add("wpLevel", wpLevel);
            dictionary.Add("wpName", wpName);
            dictionary.Add("wpDmg", wpDmg);
            dictionary.Add("wpLifes", wpLifes);
            dictionary.Add("arType", arType);
            dictionary.Add("arLevel", arLevel);
            dictionary.Add("arName", arName);
            dictionary.Add("arDmg", arDmg);
            dictionary.Add("arArmor", arArmor);

            return dictionary;
        }

        public static Dictionary<string, string> GetInventoryElements(Inventory myInventory)
        {
            string wpLevel = null;
            string wpName = null;
            string wpDmg = null;
            string wpLifes = null;
            string arLevel = null;
            string arName = null;
            string arArmor = null;
            string tempType = null;
            string tempLevel = null;
            string tempName = null;
            string tempDmg = null;
            string tempAc = null;
            string tempLifes = null;

            if (myInventory.Weapons != null)
            {
                string[] WeaponsSubstrings = myInventory.Weapons.Split(',');

                for (int i = 0; i < WeaponsSubstrings.Count(); i++)
                {
                    switch (WeaponsSubstrings[i].Split(':')[0])
                    {
                        case "level":
                            wpLevel = WeaponsSubstrings[i].Split(':')[1];
                            break;
                        case "name":
                            wpName = WeaponsSubstrings[i].Split(':')[1];
                            break;
                        case "dmg":
                            wpDmg = WeaponsSubstrings[i].Split(':')[1];
                            break;
                        case "lifes":
                            wpLifes = WeaponsSubstrings[i].Split(':')[1];
                            break;
                    }
                }
            }
            if (myInventory.Armors != null)
            {
                string[] ArmorsSubstrings = myInventory.Armors.Split(',');

                for (int i = 0; i < ArmorsSubstrings.Count(); i++)
                {
                    switch (ArmorsSubstrings[i].Split(':')[0])
                    {
                        case "level":
                            arLevel = ArmorsSubstrings[i].Split(':')[1];
                            break;
                        case "name":
                            arName = ArmorsSubstrings[i].Split(':')[1];
                            break;
                        case "dmg":
                            arArmor = ArmorsSubstrings[i].Split(':')[1];
                            break;
                    }
                }
            }
            if (myInventory.Temp != null)
            {
                string[] TempSubstrings = myInventory.Temp.Split(',');

                for (int i = 0; i < TempSubstrings.Count(); i++)
                {
                    switch (TempSubstrings[i].Split(':')[0])
                    {
                        case "type":
                            tempType = TempSubstrings[i].Split(':')[1];
                            break;
                        case "level":
                            tempLevel = TempSubstrings[i].Split(':')[1];
                            break;
                        case "name":
                            tempName = TempSubstrings[i].Split(':')[1];
                            break;
                        case "dmg":
                            tempDmg = TempSubstrings[i].Split(':')[1];
                            break;
                        case "ac":
                            tempAc = TempSubstrings[i].Split(':')[1];
                            break;
                        case "wpLifes":
                            tempLifes = TempSubstrings[i].Split(':')[1];
                            break;
                    }
                }
            }

            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("wpLevel", wpLevel);
            dictionary.Add("wpName", wpName);
            dictionary.Add("wpDmg", wpDmg);
            dictionary.Add("wpLifes", wpDmg);
            dictionary.Add("arLevel", arLevel);
            dictionary.Add("arName", arName);
            dictionary.Add("arArmor", arArmor);
            dictionary.Add("tempType", tempType);
            dictionary.Add("tempLevel", tempLevel);
            dictionary.Add("tempName", tempName);
            dictionary.Add("tempDmg", tempDmg);
            dictionary.Add("tempAc", tempAc);
            dictionary.Add("tempLifes", tempAc);

            return dictionary;
        }

        /////////// Monsters, Weapons, Armors and Caves ////////////////
        //easy monster sprite and name up to lvl 5, then medium up to lvl 10 then hard
        //fix health and exp with proper functions (see Randomization.Monster)
        public static Tuple<string, string> GetMonster(int level)
        {
            Random rnd = new Random();
            Tuple<int, int[]> monsterDetails = Monster.CreateMonster(level);

            string monsterString = $"type:monster,level:{level}," +
                                   $"exp:{monsterDetails.Item2[monsterDetails.Item2.Length - 2]}," +
                                   $"health:{monsterDetails.Item2[monsterDetails.Item2.Length - 1]}," +
                                   $"dmg:{monsterDetails.Item2[0]}-{monsterDetails.Item2[1]}," +
                                   $"ac:{monsterDetails.Item2[monsterDetails.Item1]}," +
                                   $"lifes:{monsterDetails.Item2[2]}";

            if (level < 6)
            {
                int whichEasySprite = rnd.Next(0, easyMonsters.GetLength(0));
                return Tuple.Create(easyMonsters[whichEasySprite, 0], $"{monsterString},name:{easyMonsters[whichEasySprite, 1]}");
            }
            else if (level >= 6 && level < 20)
            {
                int whichMediumSprite = rnd.Next(0, mediumMonsters.GetLength(0));
                return Tuple.Create(mediumMonsters[whichMediumSprite, 0], $"{monsterString},name:{mediumMonsters[whichMediumSprite, 1]}");
            }
            else
            {
                int whichHardSprite = rnd.Next(0, hardMonsters.GetLength(0));
                return Tuple.Create(hardMonsters[whichHardSprite, 0], $"{monsterString},name:{hardMonsters[whichHardSprite, 1]}");
            }
        }

        public static Tuple<string, string> GetWeapon(int level)
        {
            Random rnd = new Random();
            int[] weaponDetails = Weapon.CreateWeapon(level);

            string weaponString = $"type:weapon,level:{level},dmg:{weaponDetails[0]}-{weaponDetails[1]},lifes:{weaponDetails[2]}";

            if (level < 6)
            {
                int whichEasySprite = rnd.Next(0, easyWeapons.GetLength(0));
                return Tuple.Create(easyWeapons[whichEasySprite, 0], $"{weaponString},name:{easyWeapons[whichEasySprite, 1]}");
            }
            else if (level >= 6 && level < 20)
            {
                int whichMediumSprite = rnd.Next(0, mediumWeapons.GetLength(0));
                return Tuple.Create(mediumWeapons[whichMediumSprite, 0], $"{weaponString},name:{mediumWeapons[whichMediumSprite, 1]}");
            }
            else
            {
                int whichHardSprite = rnd.Next(0, hardWeapons.GetLength(0));
                return Tuple.Create(hardWeapons[whichHardSprite, 0], $"{weaponString},name:{hardWeapons[whichHardSprite, 1]}");
            }
        }

        public static Tuple<string, string> GetArmor(int level)
        {
            Random rnd = new Random();
            int[] armorDetails = Armor.CreateArmor(level);

            string armorString = $"type:armor,level:{level},ac:{armorDetails[0]}";

            if (level < 6)
            {
                int whichEasySprite = rnd.Next(0, easyArmors.GetLength(0));
                return Tuple.Create(easyArmors[whichEasySprite, 0], $"{armorString},name:{easyArmors[whichEasySprite, 1]}");
            }
            else if (level >= 6 && level < 100)
            {
                int whichMediumSprite = rnd.Next(0, mediumArmors.GetLength(0));     
                return Tuple.Create(mediumArmors[whichMediumSprite, 0], $"{armorString},name:{mediumArmors[whichMediumSprite, 1]}");
            }
            else
            {
                int whichHardSprite = rnd.Next(0, hardArmors.GetLength(0));
                return Tuple.Create(hardArmors[whichHardSprite, 0], $"{armorString},name:{hardArmors[whichHardSprite, 1]}");
            }
        }

        public static Tuple<string, string> GetCave(int level)
        {
            string caveSprite = @"Cave/cave1.png";
            double monsterChance = Progression.GetMonsterCaveChance(level);

            Random rnd = new Random();

            if (rnd.NextDouble() <= monsterChance)
                return Tuple.Create(caveSprite, GetMonster(level).Item2);
            else
            {
                if (rnd.NextDouble() <= 0.5)
                    return Tuple.Create(caveSprite, GetWeapon(level).Item2);
                else
                    return Tuple.Create(caveSprite, GetArmor(level).Item2);
            }

        }

        //////// Sprites ////////
        public static readonly string[,] easyWeapons =
        {
            { "Weapons/Easy/BlackSword.png", "Black Sword" },
            { "Weapons/Easy/IronSword.png", "Iron Sword" },
            { "Weapons/Easy/WoodenSword.png", "Wooden Sword" }
        };

        public static readonly string[,] mediumWeapons =
        {
            { "Weapons/Medium/GreatSword.png", "Great Sword" },
            { "Weapons/Medium/GreatSword2.png", "Greater Sword" },
            { "Weapons/Medium/KnightSword.png", "Knight Sword" },
            { "Weapons/Medium/PirateSword.png", "Pirate Sword" }
        };

        public static readonly string[,] hardWeapons =
        {
            { "Weapons/Hard/SunSword.png", "Sun Sword" },
            { "Weapons/Hard/NightSword.png", "Night Sword" },
            { "Weapons/Hard/EmeraldSword.png", "Emerald Sword" },
            { "Weapons/Hard/DragonSword.png", "Dragon Sword" }
        };

        public static readonly string[,] easyArmors =
{
            { "Armor/Easy/IronArmor.png", "Iron Armor" },
            { "Armor/Easy/FullIronArmor.png", "Full Iron Armor" }
        };

        public static readonly string[,] mediumArmors =
        {
            { "Armor/Medium/FullSteelArmor.png", "Full Steel Armor" },
            { "Armor/Medium/KnightArmor.png", "Knight Armor" }
        };

        public static readonly string[,] hardArmors =
        {
            { "Armor/Hard/HeroicArmor.png", "Heroic Armor" }
        };

        public static readonly string[,] easyMonsters =
        {
            { "Monsters/Easy/Centipede.png", "Centipede" },
            { "Monsters/Easy/HugeWasp.png", "Huge Wasp" },
            { "Monsters/Easy/LizardWarrior.png", "Lizard Warrior" },
            { "Monsters/Easy/MonsterTree.png", "Monster Tree" }
        };

        public static readonly string[,] mediumMonsters =
        {
            { "Monsters/Medium/Executioner.png", "Executioner" },
            { "Monsters/Medium/OrcWarrior.png", "Orc Warrior" },
            { "Monsters/Medium/SkeletonWarrior.png", "Skeleton Warrior" }
        };

        public static readonly string[,] hardMonsters =
        {
            { "Monsters/Hard/SquidWorst.png", "Great Squid Mage" },
            { "Monsters/Hard/BirdBrain.png", "Bird Brain" },
            { "Monsters/Hard/MonsterRat.png", "Monster Rat" }
        };
    }
}

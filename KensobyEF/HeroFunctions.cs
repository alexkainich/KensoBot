﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using KensobyEF.Model;
using System.Linq;
using System.Threading.Tasks;
using Randomization;

namespace KensobyEF
{
    public class HeroFunctions
    {
        public static bool IsGuildNew(string guildId)
        {
            using (var db = new KensobyContext())
            {
                var h = from p in db.Heroes where p.HeroId == guildId select p;
                if (h.Any())
                    return false;
                else
                    return true;
            }
        }

        public static void CreateNewHero(string guildId, string guildName)
        {
            using (var db = new KensobyContext())
            {
                db.Heroes.Add(new Hero
                {
                    HeroId = guildId,
                    Name = guildName,
                    Deaths = "0",
                    Level = "1",
                    Exp = Progression.GetExpPerLevel(0).ToString(),
                    ExpNext = Progression.GetExpPerLevel(1).ToString(),
                    Health = "100", //if you ever ychange this... you may also want to change the monsters starting health (see randomizationbylevel monster)
                    MaxHealth = "100",
                    Mana = "100",
                    MaxMana = "100",
                    Weapon = "type:weapon,level:1,diff:easy,name:Punch,dmg:1-1,ac:0,lifes:0",
                    Armor = "type:armor,level:1,diff:easy,name:Clothes,dmg:0,ac:1",
                    Spell = "None",
                    Effect = "None",
                    HeroSprite = @"Heroes\Hero1.png",
                    Map = new Map
                    {
                        MapId = guildId
                    },
                    Inventory = new Inventory
                    {
                        InventoryId = guildId,
                        Temp = "none",
                        Armors = "none",
                        Weapons = "none"
                    },
                    Pvp = new Pvp
                    {
                        PvpId = guildId,
                        Played = 0,
                        Won = 0,
                        Challenger = "none",
                        Message = "none"
                    }

                });
                db.SaveChanges();
            }
        }

        public static string GetHeroStats(string guildId)
        {
            Hero myHero = GetHero(new KensobyContext(), guildId);
            if (myHero != null)
            {
                Dictionary<string, string> heroElements = Conventions.GetHeroElements(myHero);

                string stats =
                    $"Name: {myHero.Name} \n" +
                    $"Level: {myHero.Level} \n" +
                    $"Experience: {myHero.Exp} / {myHero.ExpNext} \n" +
                    $"Health: {myHero.Health} / {myHero.MaxHealth} \n";
                //$"Mana: {myHero.Mana} / {myHero.MaxMana} \n" +

                string weaponstats;
                if (heroElements["wpLifes"] == "0")
                    weaponstats = $"Weapon: {heroElements["wpName"]} ({heroElements["wpLevel"]}), Damage: {heroElements["wpDmg"]} \n";
                else
                    weaponstats = $"Weapon: {heroElements["wpName"]} ({heroElements["wpLevel"]}), Damage: {heroElements["wpDmg"]}, Lifesteal: {heroElements["wpLifes"]}% \n";

                string armorstats =
                    $"Armor: {heroElements["arName"]} ({heroElements["arLevel"]}), Class: {heroElements["arArmor"]} \n";
                //$"Spell: {myHero.Spell} \n" +
                //$"Effect: {myHero.Effect} \n";

                string deathstats =
                    $"Deaths from monsters: {myHero.Deaths}";

                return stats + weaponstats + armorstats + deathstats;
            }
            else
            {
                return "";
            }
        }

        public static string GetTopHeroes(string[] guildUsers)
        {
            using (var db = new KensobyContext())
            {
                int[] levels = new int[guildUsers.Length];

                for (int i = 0; i < guildUsers.Length; i++)
                {
                    Hero h = GetHero(db, guildUsers[i]);

                    if (h != null)
                        levels[i] = Int32.Parse(h.Level);
                    else                  
                        levels[i] = -1;
                }

                Array.Sort(levels, guildUsers);
                Array.Reverse(levels);
                Array.Reverse(guildUsers);

                string reply = "Top Heroes in server:\n" +
                               "Level" + "     " + "Hero Name" + "     " + "PvPs Won\n";

                int count = 0;
                for (int i = 0; i < guildUsers.Length; i++)
                {
                    if (levels[i] != -1)
                    {
                        Pvp p = (from m in db.Pvps where m.PvpId == guildUsers[i] select m).FirstOrDefault();
                        Hero h = GetHero(db, guildUsers[i]);

                        count++;

                        reply += levels[i] + "     " + h.Name + "     " + p.Won + "\n";
                        if (count > 10)
                            break;
                    }
                }

                return reply;
            }
        }

        public static string GetTopHeroes()
        {
            using (var db = new KensobyContext())
            {
                var h = (from m in db.Heroes orderby m.Level descending select m).Take(10).ToArray();

                string reply = "Hall of Fame:\n" +
                               "Level" + "     " + "Hero Name" + "     " + "PvPs Won\n";
                foreach (var hero in h)
                {
                    Pvp p = (from m in db.Pvps where m.PvpId == hero.HeroId select m).FirstOrDefault();
                    reply += hero.Level + "     " + hero.Name + "     " + p.Won + "\n";
                }

                return reply;
            }
        }

        public static Tuple<string, string> GetHeroStatsForImage(string guildId)
        {
            Hero myHero = GetHero(new KensobyContext(), guildId);
            if (myHero != null)
            {
                Dictionary<string, string> heroElements = Conventions.GetHeroElements(myHero);

                string stats =
                    $"Level: {myHero.Level} \n" +
                    $"Experience: {myHero.Exp} / {myHero.ExpNext} \n" +
                    $"Health: {myHero.Health} / {myHero.MaxHealth} \n";
                //$"Mana: {myHero.Mana} / {myHero.MaxMana} \n" +

                string weaponstats;
                if (heroElements["wpLifes"] == "0")
                    weaponstats = $"Weapon: {heroElements["wpName"]} ({heroElements["wpLevel"]}), Damage: {heroElements["wpDmg"]} \n";
                else
                    weaponstats = $"Weapon: {heroElements["wpName"]} ({heroElements["wpLevel"]}), Damage: {heroElements["wpDmg"]}, Lifesteal: {heroElements["wpLifes"]}% \n";

                string armorstats =
                    $"Armor: {heroElements["arName"]} ({heroElements["arLevel"]}), Class: {heroElements["arArmor"]}";
                //$"Spell: {myHero.Spell} \n" +
                //$"Effect: {myHero.Effect} \n";

                return Tuple.Create($"{myHero.Name}", stats + weaponstats + armorstats);
            }
            else
            {
                return Tuple.Create("", "");
            }
        }

        public static Hero GetHero(KensobyContext db, string guildId)
        {
            Hero h = (from p in db.Heroes where p.HeroId == guildId select p).FirstOrDefault();
            return h;
        }

        public static string UpdateHeroAfterPvE(string guildId, string newHealth, int addExp, int levelDifference)
        {
            using (var db = new KensobyContext())
            {
                Hero h = (from p in db.Heroes where p.HeroId == guildId select p).First();
                Random rnd = new Random();
                string result = "none";

                if (Int32.Parse(newHealth) > 0)
                {
                    HeroLevelUp(db, h, guildId, newHealth, addExp);
                }
                else
                {
                    if (levelDifference > 0)
                    {
                        double prob = Progression.GetLoseInvProbOnDeath(Math.Min(100, levelDifference));
                        if (rnd.NextDouble() <= prob)
                        {
                            InventoryFunctions.DropArmor(guildId);
                            result += "armor";
                        }

                        prob = Progression.GetLoseInvProbOnDeath(Math.Min(100, levelDifference));
                        if (rnd.NextDouble() <= prob)
                        {
                            InventoryFunctions.DropWeapon(guildId);
                            result += "weapon";
                        }
                    }

                    HeroDead(db, h, guildId);
                }

                return result;
            }
        }

        public static Hero UpdateHeroAfterPvP(string guildId, string newHealth)
        {
            using (var db = new KensobyContext())
            {
                Hero h = (from p in db.Heroes where p.HeroId == guildId select p).First();
                h.Health = newHealth;

                db.SaveChanges();

                return h;
            }
        }

        public static Hero HeroLevelUp(KensobyContext db, Hero h, string guildId, string newHealth, int addExp)
        {
            using (db)
            {
                h.Health = newHealth;
                h.Exp = (Int32.Parse(h.Exp) + addExp).ToString();

                if (Progression.GetExpPerLevel(Int32.Parse(h.Level) + 1) <= Int32.Parse(h.Exp))
                {
                    h.Level = (Int32.Parse(h.Level) + 1).ToString();

                    h.MaxHealth = (Int32.Parse(h.MaxHealth) + 50).ToString();
                    h.MaxMana = (Int32.Parse(h.MaxMana) + 50).ToString();
                    h.Health = h.MaxHealth;
                    h.ExpNext = Progression.GetExpPerLevel(Int32.Parse(h.Level) + 1).ToString();
                    h.Mana = h.MaxMana;
                }

                db.SaveChanges();
                return h;
            }
        }

        public static Hero HeroDead(KensobyContext db, Hero h, string guildId)
        {
            using (db)
            {
                h.Health = h.MaxHealth;
                h.Mana = h.MaxMana;
                h.Deaths = (Int32.Parse(h.Deaths) + 1).ToString();
                h.Exp = Progression.GetExpPerLevel(Int32.Parse(h.Level)).ToString();

                db.SaveChanges();
            }
            return h;
        }

        public static Hero HeroDead(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Hero h = (from p in db.Heroes where p.HeroId == guildId select p).First();
                h.Health = h.MaxHealth;
                h.Mana = h.MaxMana;
                h.Deaths = (Int32.Parse(h.Deaths) + 1).ToString();
                h.Exp = Progression.GetExpPerLevel(Int32.Parse(h.Level) - 1).ToString();

                db.SaveChanges();

                return h;
            }
        }

        public static Hero HeroPvpDead(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Hero h = (from p in db.Heroes where p.HeroId == guildId select p).First();
                h.Health = h.MaxHealth;
                h.Mana = h.MaxMana;
                //h.Deaths = (Int32.Parse(h.Deaths) + 1).ToString();
                h.Exp = Progression.GetExpPerLevel(Int32.Parse(h.Level) - 1).ToString();

                db.SaveChanges();

                return h;
            }
        }

        public static async Task UpdateHealth(int minutesTime, int hoursTime)
        {
            using (var db = new KensobyContext())
            {
                foreach (Hero hero in db.Heroes.ToList())
                {
                    double temp = (double)minutesTime / (hoursTime * 60) * Double.Parse(hero.MaxHealth);
                    int healthIncrease = Math.Max(1, (int)Math.Ceiling(temp));
                    hero.Health = Math.Min(Int32.Parse(hero.Health) + healthIncrease, Int32.Parse(hero.MaxHealth)).ToString();
                }

                db.SaveChanges();
            }
        }
    }
}
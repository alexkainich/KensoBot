﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using KensobyEF.Model;
using System.Linq;
using KensobyEF;

namespace KensobyEF
{
    public class InventoryFunctions
    {

        public static string ItemPending(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Inventory i = (from p in db.Inventories where p.InventoryId == guildId select p).First();
                return i.Temp;
            }
        }

        public static Tuple<string, string> WeaponPending(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Inventory i = (from p in db.Inventories where p.InventoryId == guildId select p).First();
                return Tuple.Create(i.Weapons, i.Temp);
            }
        }

        public static Tuple<string, string> ArmorPending(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Inventory i = (from p in db.Inventories where p.InventoryId == guildId select p).First();
                return Tuple.Create(i.Armors, i.Temp);
            }
        }

        public static void UpdateHeroAfterWeapon(string guildId, string newWeapon)
        {
            using (var db = new KensobyContext())
            {
                Inventory i = (from p in db.Inventories where p.InventoryId == guildId select p).First();
                if (i.Weapons == "none")
                {
                    i.Weapons = newWeapon;
                }
                else
                {
                    i.Temp = newWeapon;
                }
                db.SaveChanges();
            }
        }

        public static void UpdateHeroAfterArmor(string guildId, string newArmor)
        {
            using (var db = new KensobyContext())
            {
                Inventory i = (from p in db.Inventories where p.InventoryId == guildId select p).First();
                if (i.Armors == "none")
                {
                    i.Armors = newArmor;
                }
                else
                {
                    i.Temp = newArmor;
                }
                db.SaveChanges();
            }
        }

        public static string GetInventoryStats(string guildId)
        {
            Inventory inv = GetInventory(new KensobyContext(), guildId);
            if (inv != null)
            {
                Dictionary<string, string> inventoryElements = Conventions.GetInventoryElements(inv);

                string weapon;
                string armor;

                weapon = (inventoryElements["wpName"] != null) ? $"{inventoryElements["wpName"]} ({inventoryElements["wpLevel"]})" : "None";
                armor = (inventoryElements["arName"] != null) ? $"{inventoryElements["arName"]} ({inventoryElements["arLevel"]})" : "None";

                string stats = $"Inventory Weapon: {weapon} \nInventory Armor: {armor}";
                return stats;
            }
            else
                return null;
        }

        public static Inventory GetInventory(KensobyContext db, string guildId)
        {
            Inventory i = (from p in db.Inventories where p.InventoryId == guildId select p).FirstOrDefault(); //will return null if empty
            return i;
        }

        public static Tuple<string, string> ReplaceInventoryItem(string guild)
        {
            using (var db = new KensobyContext())
            {
                Inventory inv = GetInventory(db, guild);
                if (inv == null)
                    return Tuple.Create("none", "none");

                Dictionary<string, string> inventoryElements = Conventions.GetInventoryElements(inv);
                string oldItemName, oldItem;
                string newItemName = inventoryElements["tempName"];
                string newItem = inv.Temp;

                if (newItem != "none")
                {
                    inv.Temp = "none";

                    if (inventoryElements["tempType"] == "weapon")
                    {
                        oldItemName = inventoryElements["wpName"];
                        oldItem = inv.Weapons;
                        inv.Weapons = newItem;
                    }
                    else
                    {
                        oldItemName = inventoryElements["arName"];
                        oldItem = inv.Armors;
                        inv.Armors = newItem;
                    }

                    db.SaveChanges();
                    return Tuple.Create(oldItemName, newItemName);
                }
                else
                {
                    return Tuple.Create("none", "none");
                }
            }
        }

        public static Tuple<string, string> ChangeItem(int whichItem, string guildId)
        {
            using (var db = new KensobyContext())
            {
                Hero h = HeroFunctions.GetHero(db, guildId);
                if (h == null)
                    return Tuple.Create("none", "none");

                Inventory inv = GetInventory(db, guildId);
                Dictionary<string, string> inventoryElements = Conventions.GetInventoryElements(inv);
                Dictionary<string, string> heroElements = Conventions.GetHeroElements(h);

                string oldItemName = (whichItem == 1) ? heroElements["wpName"] : heroElements["arName"];
                string newItemName = (whichItem == 1) ? inventoryElements["wpName"] : inventoryElements["arName"];
                string oldItem = (whichItem == 1) ? h.Weapon : h.Armor;
                string newItem = (whichItem == 1) ? inv.Weapons : inv.Armors;

                if (newItem != "none")
                {
                    if (whichItem == 1)
                    {
                        inv.Weapons = oldItem;
                        h.Weapon = newItem;
                    }
                    else
                    {
                        inv.Armors = oldItem;
                        h.Armor = newItem;
                    }

                    db.SaveChanges();
                    return Tuple.Create(oldItemName, newItemName);
                }
                else
                {
                    return Tuple.Create("none", "none");
                }
            }
        }

        public static string DropTempItem(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Inventory inv = GetInventory(db, guildId);

                string temp = inv.Temp;
                inv.Temp = "none";

                db.SaveChanges();

                return temp;
            }
        }

        public static void DropWeapon(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Inventory inv = GetInventory(db, guildId);

                inv.Weapons = "none";

                db.SaveChanges();
            }
        }

        public static void DropArmor(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Inventory inv = GetInventory(db, guildId);

                inv.Armors = "none";

                db.SaveChanges();

            }
        }
    }
}

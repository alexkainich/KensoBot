﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using KensobyEF.Model;
using System.Linq;
using KensobyEF;
using Randomization;

namespace KensobyEF
{
    public class MapFunctions
    {
        public static bool GuildMapLoaded(string guildId)
        {
            using (var db = new KensobyContext())
            {
                var c = from p in db.Maps where p.MapId == guildId select p;
                if (c.Any())
                {
                    if (c.First().Middle == null)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
        }

        public static Map CreateVeryFirstMap(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Map c = (from x in db.Maps where x.MapId == guildId select x).First();

                c.SpriteOne = @"Monsters/Easy/MonsterTree.png"; //choices have to align with sprite order
                c.SpriteTwo = @"Weapons/Easy/IronSword.png"; //choices have to align with sprite order
                c.SpriteThree = null; //choices have to align with sprite order
                c.SpriteFour = null;
                c.Background = @"Backgrounds/back2.png";
                c.Middle = @"Heroes/hero2.png";
                c.ChooseOne = "type:monster,level:1,name:Tree Monster,health:20,dmg:1-2,ac:0,exp:1,lifes:0";
                c.ChooseTwo = "type:weapon,level:1,name:Iron Sword,dmg:1-2,lifes:0";
                c.ChooseThree = null;
                c.ChooseFour = null;

                db.SaveChanges();
                return c;
            }
        }

        //Random map generation
        public static Map CreateNewMap(KensobyContext db, string guildId, string level)
        {
            string myLevel;
            if (level == "none")
            {
                Hero myHero = HeroFunctions.GetHero(db, guildId);
                myLevel = myHero.Level;
            }
            else
                myLevel = level;

            Random rnd = new Random();
            int[] mapchoices = Progression.GetMapChoices(Int32.Parse(myLevel));

            //Gets the sprite and the stats of each choice
            Tuple<string, string> choiceOne = CreateChoice(mapchoices[0], Int32.Parse(myLevel));
            Tuple<string, string> choiceTwo = CreateChoice(mapchoices[1], Int32.Parse(myLevel));
            Tuple<string, string> choiceThree = CreateChoice(mapchoices[2], Int32.Parse(myLevel));

            //Map stored in database for future reference (in case user logs out for example)
            Map c = (from x in db.Maps where x.MapId == guildId select x).First();
            c.SpriteOne = choiceOne.Item1;
            c.SpriteTwo = choiceTwo.Item1;
            c.SpriteThree = choiceThree.Item1;
            c.SpriteFour = null;
            c.ChooseOne = choiceOne.Item2;
            c.ChooseTwo = choiceTwo.Item2;
            c.ChooseThree = choiceThree.Item2;
            c.ChooseFour = null;
            c.Background = randomBackground();
            c.Middle = @"Heroes/hero2.png";

            db.SaveChanges();

            return c;
        }

        private static Tuple<string, string> CreateChoice(int choice, int level)
        {
            //gets the sprite filename and details for the choice to be stored in the database
            if (choice == 1)
            {
                int weaponLevel = Progression.GetNextWeaponLevel(level);
                return Conventions.GetWeapon(weaponLevel);
            }
            else if (choice == 2)
            {
                int armorLevel = Progression.GetNextArmorLevel(level);
                return Conventions.GetArmor(armorLevel);
            }
            else if (choice == 3)
            {
                int monsterLevel = Progression.GetNextMonsterLevel(level);
                return Conventions.GetMonster(monsterLevel);
            }
            else
            {
                int caveLevel = Progression.GetNextCaveLevelDiff(level);
                return Conventions.GetCave(level + caveLevel);
            }
        }

        public static Map GetMap(KensobyContext db, string guildId)
        {
            Map c = (from p in db.Maps where p.MapId == guildId select p).FirstOrDefault();
            return c;
        }

        public static Map MapCompleted(string guildId, string state)
        {
            using (var db = new KensobyContext())
            {
                Map c = (from p in db.Maps where p.MapId == guildId select p).First();

                c.Middle = null;
                db.SaveChanges();
                return c;
            }
        }

        public static string randomBackground()
        {
            Random rnd = new Random();
            return $@"Backgrounds/back{rnd.Next(1, 5)}.png";
        }
    }
}
﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace KensobyEF.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pvps",
                columns: table => new
                {
                    PvpId = table.Column<string>(nullable: false),
                    Challenger = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Played = table.Column<int>(nullable: false),
                    Won = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pvps", x => x.PvpId);
                });

            migrationBuilder.CreateTable(
                name: "Heroes",
                columns: table => new
                {
                    HeroId = table.Column<string>(nullable: false),
                    Armor = table.Column<string>(nullable: true),
                    Deaths = table.Column<string>(nullable: true),
                    Effect = table.Column<string>(nullable: true),
                    Exp = table.Column<string>(nullable: true),
                    ExpNext = table.Column<string>(nullable: true),
                    Health = table.Column<string>(nullable: true),
                    HeroSprite = table.Column<string>(nullable: true),
                    Level = table.Column<string>(nullable: true),
                    Mana = table.Column<string>(nullable: true),
                    MaxHealth = table.Column<string>(nullable: true),
                    MaxMana = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PvpId = table.Column<string>(nullable: true),
                    Spell = table.Column<string>(nullable: true),
                    Weapon = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Heroes", x => x.HeroId);
                    table.ForeignKey(
                        name: "FK_Heroes_Pvps_PvpId",
                        column: x => x.PvpId,
                        principalTable: "Pvps",
                        principalColumn: "PvpId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Inventories",
                columns: table => new
                {
                    InventoryId = table.Column<string>(nullable: false),
                    Armors = table.Column<string>(nullable: true),
                    Extra = table.Column<string>(nullable: true),
                    Spells = table.Column<string>(nullable: true),
                    Temp = table.Column<string>(nullable: true),
                    Weapons = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventories", x => x.InventoryId);
                    table.ForeignKey(
                        name: "FK_Inventories_Heroes_InventoryId",
                        column: x => x.InventoryId,
                        principalTable: "Heroes",
                        principalColumn: "HeroId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Maps",
                columns: table => new
                {
                    MapId = table.Column<string>(nullable: false),
                    Background = table.Column<string>(nullable: true),
                    ChooseFour = table.Column<string>(nullable: true),
                    ChooseOne = table.Column<string>(nullable: true),
                    ChooseThree = table.Column<string>(nullable: true),
                    ChooseTwo = table.Column<string>(nullable: true),
                    Middle = table.Column<string>(nullable: true),
                    SpriteFour = table.Column<string>(nullable: true),
                    SpriteOne = table.Column<string>(nullable: true),
                    SpriteThree = table.Column<string>(nullable: true),
                    SpriteTwo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Maps", x => x.MapId);
                    table.ForeignKey(
                        name: "FK_Maps_Heroes_MapId",
                        column: x => x.MapId,
                        principalTable: "Heroes",
                        principalColumn: "HeroId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Heroes_PvpId",
                table: "Heroes",
                column: "PvpId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Inventories");

            migrationBuilder.DropTable(
                name: "Maps");

            migrationBuilder.DropTable(
                name: "Heroes");

            migrationBuilder.DropTable(
                name: "Pvps");
        }
    }
}

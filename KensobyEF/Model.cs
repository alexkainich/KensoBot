﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using KensobyEF.Model;

namespace KensobyEF
{
    public class KensobyContext : DbContext
    {
        public DbSet<Map> Maps { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<Hero> Heroes { get; set; }
        public DbSet<Pvp> Pvps { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=Kensoby.db");
        }

        //Establish 1-1 relations
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hero>()
                .HasOne(a => a.Inventory)
                .WithOne(b => b.Hero)
                .HasForeignKey<Inventory>(b => b.InventoryId);

            modelBuilder.Entity<Hero>()
                .HasOne(a => a.Map)
                .WithOne(b => b.Hero)
                .HasForeignKey<Map>(b => b.MapId);
        }
    }
}
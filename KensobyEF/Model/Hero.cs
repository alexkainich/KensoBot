﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace KensobyEF.Model
{
    public class Hero
    {
        public string HeroId { get; set; }
        public string Name { get; set; }
        //public string Discriminator { get; set; }

        public string Deaths { get; set; }

        public string Level { get; set; }
        public string Health { get; set; }
        public string MaxHealth { get; set; }
        public string Mana { get; set; }
        public string MaxMana { get; set; }
        public string Exp { get; set; }
        public string ExpNext { get; set; }

        public string Weapon { get; set; }
        public string Armor { get; set; }
        public string Spell { get; set; }

        public string Effect { get; set; }

        public string HeroSprite { get; set; }

        public Inventory Inventory { get; set; }
        public Map Map { get; set; }
        public Pvp Pvp { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace KensobyEF.Model
{
    public class Inventory
    {
        public string InventoryId { get; set; }
        public string Weapons { get; set; }
        public string Armors { get; set; }
        public string Spells { get; set; }
        public string Temp { get; set; }
        public string Extra { get; set; }

        public Hero Hero { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace KensobyEF.Model
{
    public class Map
    {
        public string MapId { get; set; }
        public string SpriteOne { get; set; }
        public string SpriteTwo { get; set; }
        public string SpriteThree { get; set; }
        public string SpriteFour { get; set; }
        public string Background { get; set; }
        public string Middle { get; set; }

        public string ChooseOne { get; set; }
        public string ChooseTwo { get; set; }
        public string ChooseThree { get; set; }
        public string ChooseFour { get; set; }

        public Hero Hero { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace KensobyEF.Model
{
    public class Pvp
    {
        public string PvpId { get; set; }

        public int Played { get; set; }
        public int Won { get; set; }

        public string Challenger { get; set; }
        public string Message { get; set; }

        public Hero Hero { get; set; }
    }
}
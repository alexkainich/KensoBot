﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using KensobyEF.Model;
using System.Linq;

namespace KensobyEF
{
    public class PvpFunctions
    {
        public static Tuple<string, string, string> IsPvpMessagePending(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Pvp pvp = (from p in db.Pvps where p.PvpId == guildId select p).First();

                if (pvp.Message == "challenged" || pvp.Message == "pending")
                {
                    Hero heroChallenger = (from p in db.Heroes where p.HeroId == pvp.Challenger select p).First();

                    return Tuple.Create((pvp.Message == "challenged") ? "challenged" : "pending", heroChallenger.Name, heroChallenger.Level);
                }
                else if (pvp.Message == "won" || pvp.Message == "lost")
                {
                    Hero heroChallenger = (from p in db.Heroes where p.HeroId == pvp.Challenger select p).First();

                    string tempMessage = pvp.Message;
                    pvp.Challenger = "none";
                    pvp.Message = "none";
                    db.SaveChanges();

                    return Tuple.Create((tempMessage == "won") ? "won" : "lost", heroChallenger.Name, "none");
                }
                else
                {
                    return Tuple.Create("none", "none", "none");
                }
            }
        }

        public static string SendInvite(KensobyContext db, Pvp pvp, string guildId, string challenged)
        {
            Pvp pvpChallenged = (from p in db.Pvps where p.PvpId == challenged select p).FirstOrDefault();
            if (pvpChallenged == null)
                return "none";
            else if (pvpChallenged.Challenger != "none")
            {
                return "other";
            }
            else
            {
                pvp.Challenger = challenged;
                pvp.Message = "challenged";
                pvpChallenged.Challenger = guildId;
                pvpChallenged.Message = "pending";

                db.SaveChanges();

                return "done";
            }
        }

        public static void UpdatePvP(string guildId, string result)
        {
            using (var db = new KensobyContext())
            {
                Pvp pvp = (from p in db.Pvps where p.PvpId == guildId select p).First();

                if (result == "won")
                {
                    pvp.Challenger = "none";
                    pvp.Message = "none";
                    pvp.Won++;
                    pvp.Played++;
                }
                else
                {
                    pvp.Challenger = "none";
                    pvp.Message = "none";
                    pvp.Played++;
                }

                db.SaveChanges();

            }
        }

        public static void UpdatePvPChallenger(string guildIdChallenger, string result)
        {
            using (var db = new KensobyContext())
            {
                Pvp pvpChallenger = (from p in db.Pvps where p.PvpId == guildIdChallenger select p).First();

                if (result == "won")
                {
                    pvpChallenger.Message = "won";
                    pvpChallenger.Won++;
                    pvpChallenger.Played++;
                }
                else
                {
                    pvpChallenger.Message = "lost";
                    pvpChallenger.Played++;
                }

                db.SaveChanges();
            }
        }

        public static string ChallengePlayer(string guildId, string challenged)
        {
            using (var db = new KensobyContext())
            {
                Pvp pvp = (from p in db.Pvps where p.PvpId == guildId select p).First();
                if (challenged == guildId)
                {
                    return "self";
                }
                else if (pvp.Challenger == "none")
                {
                    return SendInvite(db, pvp, guildId, challenged);
                }
                else if (pvp.Challenger == challenged)
                {
                    return "same";
                }
                else if (pvp.Message == "pending")
                {
                    return "pending";
                }
                else
                {
                    return "something";
                }
            }
        }

        public static string AcceptPvp(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Pvp pvp = (from p in db.Pvps where p.PvpId == guildId select p).First();
                if (pvp.Message == "pending")
                {
                    string challenger = pvp.Challenger;
                    pvp.Challenger = "none";
                    pvp.Message = "none";

                    db.SaveChanges();

                    return challenger;
                }
                else
                    return "none";
            }
        }

        public static string RejectPvp(string guildId)
        {
            using (var db = new KensobyContext())
            {
                Pvp pvp = (from p in db.Pvps where p.PvpId == guildId select p).First();
                if (pvp.Message == "pending" || pvp.Message == "challenged")
                {
                    string challenger = pvp.Challenger;
                    pvp.Challenger = "none";
                    pvp.Message = "none";

                    Pvp pvpChallenged = (from p in db.Pvps where p.PvpId == challenger select p).First();
                    pvpChallenged.Challenger = "none";
                    pvpChallenged.Message = "none";

                    db.SaveChanges();

                    return challenger;
                }
                else
                    return "none";
            }
        }

        public static string GetPvpStats(string guildId)
        {
            Pvp myPvp = GetPvp(new KensobyContext(), guildId);
            if (myPvp != null)
            {
                string stats =
                    $"Played: {myPvp.Played} \n" +
                    $"Won: {myPvp.Won}";              

                return stats;
            }
            else
            {
                return "";
            }
        }

        public static Pvp GetPvp(KensobyContext db, string guildId)
        {
            Pvp p = (from m in db.Pvps where m.PvpId == guildId select m).FirstOrDefault();
            return p;
        }
    }
}
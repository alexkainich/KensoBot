# Kensoby

## Kensoby is a rogue-like, hero progression and  level grinding game with pvp functionality.

### Kensoby Game features:
- Kensoby is a Discord bot that allows the users to level-up their own hero.
- It generates graphics by using ImageSharp for .net core.
- Discord users can create their own hero by sending a PM to the bot, or play with the server's hero. However, the server's hero can be controlled by any user of that server. 
- Infinite leveling with randomly generated weapons, armors, monsters and caves.
- Weapons currently have: damage range, lifesteal (other ideas for the future are: poison, stun, dodge and strength over element.
- Armors currently have: armor class (other ideas for the future are: return damage, strength over element).
- Monsters currently have: a weapon and an armor of the level of the map they are in.
- Caves have a chance of containing a high level item or a high level monster.
- On death the player loses some experience but cannot lose a level.
- Inventory: 1 slot of an extra weapon and 1 slot for an extra armor.
- PvP with an invitation system. The challenged player can either accept or reject the invitation.

### Kensoby Screenshots
<img src="ReadmeImages/screenshot1.png" width="300" height="300">
<img src="ReadmeImages/screenshot2.png" width="300" height="300">

### Kensoby User experience
/k play: We are presented with the very first Map. It has 2 choices. A monster tree and a sword.
/k move 1: Decision to attack the tree (first choice)
Player wins the fight and gains enough xp to level up ! So we go from level 1 to 2. Maximum health increases and our hero has now full health.

/k play: A new random map is generated. This time with three choices: a 2-level monster, a 20-level cave and a 2-level sword.
The cave is a double edged sword. It may contain a higher level item, or a higher level monster.
/k move 3: We play it safe and grab the sword.
/k inv weapon: We equip the sword
/k stats: We can see how powerful our new weapon is.

/k play: 3 new choices: a 2-level sword, a 2-level monster and a 30-level cave.
/k move 1: We are greedy and want more weapons.
The bot informs us that there is another weapon in our 1-slot inventory (one for a weapon and one for an armor).
/k keep: We keep the new weapon and drop the "punch" that is stored in our inventory.
/k inv weapon: We equip the new weapon.

/k play: 3 new choices: a 2-level monster, a 2-level sword and another 2-level monster.
We have enough weapons so we choose the monster
/k move 1: We win our life drops and our hero goes to level 3! So maximum health goes up and current health goes to max.

/k play: 3 new choices: a 3-level monster, a 3-level monster and a 10-level cave.
/k move 3: Risky move as the monster hidden inside the cage kills our hero!
We cheat death and come back to life. But any gained xp after our current level is reduced to 0 (in this case it was 0 anyway).

/k play 2: Scared from our last map, we choose a map of level 2. We are presented with a 2-level monster, a 2-level monster and a 2-level sword.
/k move 2: Winning the monster easily and earning some xp

To be continued ...

### Kensoby Game Theory

<img src="ReadmeImages/ExcelImage.jpg">
(more details in the excel file)
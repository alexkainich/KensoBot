﻿using System;
using System.Collections.Generic;

namespace Randomization
{
    public class Armor
    {
        //Test weapon creation by level
        //while (true)
        //{
        //    int level = Int32.Parse(Console.ReadLine());
        //    if (level >= 2069)
        //    {
        //        Console.WriteLine("Weapon level is too large!");
        //    }
        //    else
        //    {
        //        Console.WriteLine("Damage, Poison %, Stun %, Critical %");
        //        for (int i = 1; i <= 30; i++)
        //        {
        //            int[] attrScores = CreateWeapon(level);
        //            if (attrScores[0] < attrScores[1])
        //                Console.WriteLine($"{attrScores[0]}-{attrScores[1]}, {attrScores[2]}, {attrScores[3]}, {attrScores[4]}");
        //            else
        //                Console.WriteLine($"{attrScores[1]}-{attrScores[0]}, {attrScores[2]}, {attrScores[3]}, {attrScores[4]}");
        //        }
        //    }
        //}

        ////Test hitting with weapon of level #
        //while (true)
        //{
        //    int level = Int32.Parse(Console.ReadLine());
        //    if (level >= 2069)
        //    {
        //        Console.WriteLine("Weapon level is too large!");
        //    }
        //    else
        //    {
        //        int[] attrScores = CreateWeapon(level);

        //        for (int i = 1; i <= 30; i++)
        //        {
        //            Console.WriteLine(WeaponHit(attrScores));
        //        }
        //    }
        //}

        //same as above but returns string
        public static int[] CreateArmor(int level)
        {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //must be kept within a linear logic (if level goes up so does the armor quality)
            //then the level progression happens in the progression class
            //probabilities must sum up to 1
            double returnDmgProb = (Math.Min(level / 20, 1) == 1) ? 0.1 : 0;
            double dodgeProb = (Math.Min(level / 10, 1) == 1) ? 0.1 : 0;
            double acProb = 1 - returnDmgProb - dodgeProb;

            int ac = 0;
            int returnDmg = 0;
            int dodge = 0;

            int acMax = 2000;
            int returnMax = 5;
            int dodgeMax = 10;

            Dictionary<int, Tuple<double, int, int>> attributes = new Dictionary<int, Tuple<double, int, int>>
            {
                {1, Tuple.Create(acProb,ac,acMax)},
                {2, Tuple.Create(returnDmgProb,returnDmg,returnMax)},
                {3, Tuple.Create(dodgeProb,dodge,dodgeMax)}
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            };

            int maxScore = 0;
            int totalInitial = 0;
            foreach (var item in attributes)
            {
                maxScore += item.Value.Item3;
                totalInitial += item.Value.Item2;
            }
            if (level > maxScore - totalInitial)
                return CalculateScore(maxScore - totalInitial, attributes);
            else
                return CalculateScore(level, attributes);
        }

        //item1: attribute probability, Item2: attribute initial, Item3: attribute maximum
        private static int[] CalculateScore(int level, Dictionary<int, Tuple<double, int, int>> attributes)
        {
            int[] calibrations = new int[attributes.Count];
            for (int i = 0; i < calibrations.Length; i++)
                calibrations[i] = attributes[i + 1].Item2;

            int totalScore = 0;
            Random rnd = new Random();
            while (totalScore < level)
            {
                int chosenAttr = RandomPick(attributes);
                if (calibrations[chosenAttr] < attributes[chosenAttr + 1].Item3)
                    calibrations[chosenAttr] += 1;

                totalScore = 0;
                for (int i = 0; i < calibrations.Length; i++)
                {
                    totalScore += calibrations[i];
                    totalScore -= attributes[i + 1].Item2;
                }
            }

            return calibrations;
        }

        private static int RandomPick(Dictionary<int, Tuple<double, int, int>> probabilities)
        {
            Random r = new Random();
            double diceRoll = r.NextDouble();

            int randompick = 0;
            double cumulativeProb = 0.0;
            foreach (KeyValuePair<int, Tuple<double, int, int>> prob in probabilities)
            {
                cumulativeProb += prob.Value.Item1;
                if (diceRoll < cumulativeProb)
                {
                    randompick = prob.Key - 1;
                    break;
                }
            }

            return randompick;
        }

    }
}
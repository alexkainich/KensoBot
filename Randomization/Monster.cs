﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Randomization
{
    public class Monster
    {
        public static Tuple<int, int[]> CreateMonster(int level)
        {
            int[] MonWeapon = Weapon.CreateWeapon(Progression.GetNextWeaponLevel(level));
            int[] MonArmor = Armor.CreateArmor(Progression.GetNextArmorLevel(level));

            return MonsterArray(level, MonWeapon, MonArmor);
        }

        public static Tuple<int, int[]> MonsterArray(int level, int[] MonWeapon, int[] MonArmor)
        {
            int[] weapon = MonWeapon;
            int[] armor = MonArmor;
            int health = Progression.GetMonsterHealth(level);
            int exp = Progression.GetMonsterExp(level, health);

            int[] combined = new int[weapon.Length + armor.Length + 2];

            for (int i = 0; i < weapon.Length; i++)
            {
                combined[i] = weapon[i];
            }

            for (int i = 0; i < armor.Length; i++)
            {
                combined[weapon.Length + i] = armor[i];
            }

            combined[weapon.Length + armor.Length] = exp;
            combined[weapon.Length + armor.Length + 1] = health;

            return Tuple.Create(weapon.Length, combined);
        }
    }
}

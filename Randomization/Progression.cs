﻿using System;
using System.Collections.Generic;
using System.Text;
using Accord.Statistics.Distributions.Univariate;

namespace Randomization
{
    //sprites (which also depend on level) are determined by the KensobyEF.Conventions class
    //input should be requested map's level
    public class Progression
    {
        public static int GetNextWeaponLevel(int level)
        {
            return level;
        }

        public static double GetWeaponAppearChance(int level)
        {
            LognormalDistribution lognormal = new LognormalDistribution(0.0, 2.0);
            double point = level / 100.0;
            double pointProb = lognormal.DistributionFunction(point);
            double result = (1 - pointProb) * 0.165; //3 choices on each map: item(armor or weapon), monster, cave. 33% chance each

            return result;
        }

        public static double GetWeaponCaveChance(int level)
        {
            double result = GetWeaponAppearChance(level) / 10;

            return result;
        }

        public static int GetNextArmorLevel(int level)
        {
            int result = Math.Max(1, level / 2);

            return result;
        }

        public static double GetArmorAppearChance(int level)
        {
            double result = GetWeaponAppearChance(level);

            return result;
        }

        public static double GetArmorCaveChance(int level)
        {
            double result = GetWeaponCaveChance(level);

            return result;
        }

        public static int GetNextMonsterLevel(int level)
        {
            int result = level;

            return result;
        }

        public static int GetMonsterHealth(int level)
        {
            LognormalDistribution lognormal = new LognormalDistribution(-0.5, 0.7);
            double sample = lognormal.Generate();
            int result = (int)(GetHeroHealthPerLevel(level) * Math.Max(0.10, sample * 20.0 / 34.0));

            return result;
        }

        public static int GetMonsterExp(int level, int health)
        {
            int result = Math.Max(1, level);

            return result;
        }

        public static double GetMonsterAppearChance(int level)
        {
            double result = 1 - GetWeaponAppearChance(level) - GetArmorAppearChance(level) - GetCaveAppearChance(level);

            return result;
        }

        public static double GetMonsterCaveChance(int level)
        {
            double result = 1 - GetWeaponCaveChance(level) - GetArmorCaveChance(level);

            return result;
        }

        //returns the level difference between the hero and the cave
        public static int GetNextCaveLevelDiff(int level)
        {
            LognormalDistribution lognormal = new LognormalDistribution(-2.0, 0.7);
            double point = lognormal.Generate();
            int result = (int)(point * 100 - 5);

            return result;
        }

        public static double GetCaveAppearChance(int level)
        {
            return 0.3333;
        }

        //"level" is the level difference between the hero and the monster
        public static double GetLoseInvProbOnDeath(int level)
        {
            double result = level / 100.0;

            return result;
        }

        public static int[] GetMapChoices(int level)
        {
            //Number of choices and what they are
            //1:item, 2:Monster or 3:Cave
            //Uses a uniform probability distribution
            Random rnd = new Random();
            int ch1, ch2, ch3;
            double[] probs = { GetWeaponAppearChance(level), GetArmorAppearChance(level), GetMonsterAppearChance(level), GetCaveAppearChance(level) };
            ch1 = RandomPick(probs);

            if (ch1 == 1 || ch1 == 2)
            {
                ch2 = rnd.Next(3, 5);
                ch3 = (ch2 == 4) ? 3 : rnd.Next(3, 5);
            }
            else
            {
                ch2 = (ch1 == 4) ? rnd.Next(1, 4) : rnd.Next(1, 5);

                if (ch2 == 1 || ch2 == 2)
                    ch3 = (ch1 == 4 || ch2 == 4) ? 3 : rnd.Next(3, 5);
                else
                    ch3 = (ch1 == 4 || ch2 == 4) ? rnd.Next(1, 4) : rnd.Next(1, 5);
            }

            return new int[] { ch1, ch2, ch3 };
        }

        public static int GetExpPerLevel(string level)
        {
            int myLevel;
            Int32.TryParse(level, out myLevel);
            int result = 0;

            if (myLevel > 0)
            {
                for (int i = 1; i < myLevel; i++)
                {
                    result += i * Math.Max(1, i / 3);
                }
            }

            return result;
        }

        public static int GetExpPerLevel(int level)
        {
            int myLevel = level;
            int result = 0;

            if (myLevel > 0)
            {
                for (int i = 1; i < myLevel; i++)
                {
                    result += i * Math.Max(1, i / 3);
                }
            }

            return result;
        }

        public static int GetHeroHealthPerLevel(int level)
        {
            int result = 100 + (level - 1) * 50;

            return result;
        }

        private static int RandomPick(int[] probabilities)
        {
            Random r = new Random();
            double diceRoll = r.Next(1, 101);

            int randompick = 0;
            int cumulativeProb = 0;
            for (int i = 0; i < probabilities.Length; i++)
            {
                cumulativeProb += probabilities[i];
                if (diceRoll < cumulativeProb)
                {
                    randompick = i + 1;
                    break;
                }
            }

            return randompick;
        }

        private static int RandomPick(double[] probabilities)
        {
            Random r = new Random();
            double diceRoll = r.NextDouble();

            int randompick = 0;
            double cumulativeProb = 0;
            for (int i = 0; i < probabilities.Length; i++)
            {
                cumulativeProb += probabilities[i];
                if (diceRoll < cumulativeProb)
                {
                    randompick = i + 1;
                    break;
                }
            }

            return randompick;
        }
    }
}
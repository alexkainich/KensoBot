﻿using System;
using System.Collections.Generic;

namespace Randomization
{
    public class Weapon
    {
        //Test weapon creation by level
        //while (true)
        //{
        //    int level = Int32.Parse(Console.ReadLine());
        //    if (level >= 2069)
        //    {
        //        Console.WriteLine("Weapon level is too large!");
        //    }
        //    else
        //    {
        //        Console.WriteLine("Damage, Poison %, Stun %, Critical %");
        //        for (int i = 1; i <= 30; i++)
        //        {
        //            int[] attrScores = CreateWeapon(level);
        //            if (attrScores[0] < attrScores[1])
        //                Console.WriteLine($"{attrScores[0]}-{attrScores[1]}, {attrScores[2]}, {attrScores[3]}, {attrScores[4]}");
        //            else
        //                Console.WriteLine($"{attrScores[1]}-{attrScores[0]}, {attrScores[2]}, {attrScores[3]}, {attrScores[4]}");
        //        }
        //    }
        //}

        ////Test hitting with weapon of level #
        //while (true)
        //{
        //    int level = Int32.Parse(Console.ReadLine());
        //    if (level >= 2069)
        //    {
        //        Console.WriteLine("Weapon level is too large!");
        //    }
        //    else
        //    {
        //        int[] attrScores = CreateWeapon(level);

        //        for (int i = 1; i <= 30; i++)
        //        {
        //            Console.WriteLine(WeaponHit(attrScores));
        //        }
        //    }
        //}

        //for testing only. Not used currently
        public static string WeaponHit(int[] result)
        {
            Random rnd = new Random();

            //Starting Weapon Damage
            int dmg;
            if (result[0] < result[1])
                dmg = rnd.Next(result[0], result[1] + 1);
            else
                dmg = rnd.Next(result[1], result[0] + 1);

            //Applying Critical x2
            dmg = (rnd.Next(1, 101) <= result[4]) ? dmg * 2 : dmg;

            //Applying Effects
            string effect = "";
            effect += (rnd.Next(1, 101) <= result[3]) ? "stun" : "";
            effect += (rnd.Next(1, 101) <= result[2]) ? "dot" : "";

            return $"Damage: {dmg}, Effect: {effect}";
        }

        //same as above but returns string
        public static int[] CreateWeapon(int level)
        {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //must be kept within a linear logic (if level goes up so does the armor quality)
            //then the level progression happens in the progression class
            //probabilities must sum up to 1
            double mindmgProb = 0.45;
            double maxdmgProb = 0.45;
            double lifestealProb = 0.10;

            int mindmg = 1;
            int maxdmg = 1;
            int lifesteal = 0;

            int mindmgMax = 1000;
            int maxdmgMax = 1000;
            int lifestealMax = 10;

            Dictionary<int, Tuple<double, int, int>> attributes = new Dictionary<int, Tuple<double, int, int>>
            {
                {1, Tuple.Create(mindmgProb,mindmg,mindmgMax)},
                {2, Tuple.Create(maxdmgProb,maxdmg,maxdmgMax)},
                {3, Tuple.Create(lifestealProb,lifesteal,lifestealMax)}
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            };

            int[] result; ;
            int maxScore = 0;
            int totalInitial = 0;
            foreach (var item in attributes)
            {
                maxScore += item.Value.Item3;
                totalInitial += item.Value.Item2;
            }
            if (level > maxScore - totalInitial)
                result = CalculateScore(maxScore - totalInitial, attributes);
            else
                result = CalculateScore(level, attributes);

            if (result[0] <= result[1])
            {
                return result;
            }
            else
            {
                int temp = result[0];
                result[0] = result[1];
                result[1] = temp;

                return result;
            }
        }

        //item1: attribute probability, Item2: attribute initial, Item3: attribute maximum
        public static int[] CalculateScore(int level, Dictionary<int, Tuple<double, int, int>> attributes)
        {
            int[] calibrations = new int[attributes.Count];
            for (int i = 0; i < calibrations.Length; i++)
                calibrations[i] = attributes[i + 1].Item2;

            int totalScore = 0;
            Random rnd = new Random();
            while (totalScore < level)
            {
                int chosenAttr = RandomPick(attributes);
                if (calibrations[chosenAttr] < attributes[chosenAttr + 1].Item3)
                    calibrations[chosenAttr] += 1;

                totalScore = 0;
                for (int i = 0; i < calibrations.Length; i++)
                {
                    totalScore += calibrations[i];
                    totalScore -= attributes[i + 1].Item2;
                }
            }

            return calibrations;
        }

        private static int RandomPick(Dictionary<int, Tuple<double, int, int>> probabilities)
        {
            Random r = new Random();
            double diceRoll = r.NextDouble();

            int randompick = 0;
            double cumulativeProb = 0.0;
            foreach (KeyValuePair<int, Tuple<double, int, int>> prob in probabilities)
            {
                cumulativeProb += prob.Value.Item1;
                if (diceRoll < cumulativeProb)
                {
                    randompick = prob.Key - 1;
                    break;
                }
            }

            return randompick;
        }

    }
}
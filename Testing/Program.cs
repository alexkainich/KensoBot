﻿using System;
using Fighting;
using Kensoby;
using Randomization;
using KensobyEF.Model;

namespace Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            int level;
            Int32.TryParse(Console.ReadLine(), out level);
            if (level <= 0)
            {
                Console.WriteLine("need integer for the level");
            }

            //test 20 weapons
            for (int i = 1; i < 30; i++)
            {
                int[] HeroWeapon = Weapon.CreateWeapon(level);
                string heroDmg = $"{HeroWeapon[0]}-{HeroWeapon[1]}";
                int[] myHero = new int[] { 1000, 1000, 5, HeroWeapon[2], 5 };

                //Hero vs 1000 Monsters
                int wins = 0;
                for (int j = 1; j <= 1000; j++)
                {
                    int[] MonWeapon = Weapon.CreateWeapon(level);
                    string monDmg = $"{MonWeapon[0]}-{MonWeapon[1]}";
                    int[] myMonst = new int[] { 1000, 1000, 5, MonWeapon[2], 5 };

                    Tuple<int, int> whoWon = Fight.fight(myHero, heroDmg, myMonst, monDmg);
                    if (whoWon.Item2 == 0)
                        wins++;
                }

                Console.WriteLine($"Weapon with damage: {heroDmg} and lifesteal: {HeroWeapon[2]} has score: {wins} / 1000");
            }

            Console.ReadLine();
        }
    }
}
